A CUPS filter for Brother QL series label printers
==================================================

The Brother QL series of label printers are easy to use printers. You can use
pre-cut labels or create your own with continuous lenght labels.

This CUPS filter implementation will add a bi-colour print capability for the
QL8xx series printers in conjunction with the special DK2215 roll.

Read more online: https://flap.codeberg.page/cups-rastertoql/

Or local in *doc/html/index.html*

Happy printing.

Debian build
============

For details refer https://www.debian.org/doc/manuals/maint-guide/dreq.html

To build the package run:

    $ cd cups-rastertoql
    $ debuild -i -I -us -uc

/*
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * (c) 2021 Juergen Borleis <projects@caps-printing.org>
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 */
#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <cups/ppd.h>
#include <cups/raster.h>

#include "qldriver.h"

/**
 * @file cupsbackend.c
 * @brief Generic functions making use of CUPS
 */
extern int terminate;

/**
 * Send data to the printer's stream
 * @param[in,out] ql Full job description
 * @param[in] data Pointer to the to be sent data
 * @param[in] count Count of bytes @b data points to
 * @param[in] flush '1' if the stream should be flushed
 * @retval 0 On success
 * @retval Negative ERRNO from @b fwrite() call
 */
int ql_printer_data_send(struct qldriver *ql __attribute__((unused)), const void *data, size_t count, int flush)
{
	size_t cnt;

	cnt = fwrite(data, 1, count, stdout);
	if (cnt != count) {
		/* failed to send the command! We are on fire! */
		return -errno;
	}

	if (flush)
		fflush(stdout);

	return 0;
}

/**
 * Read in the next line from the CUPS raster
 * @param[in,out] ql Full job description
 * @param[in] sz Size of bytes @b buf points to
 * @param[out] buf Where to store the raster data
 * @retval 0 On success
 * @retval -ENODATA Mature end of CUPS raster input data
 */
int cups_ql_next_line_read(struct qldriver *ql, size_t sz, unsigned char buf[sz])
{
	unsigned cnt;

	assert(sz <= UINT_MAX);

	/* add data from the CUPS raster data */
	cnt = cupsRasterReadPixels(ql->ras, buf, (unsigned)sz);
	if (cnt != (unsigned)sz) {
		fprintf(stderr, "DEBUG: Failed to read next line of data from the CUPS raster file\n"); /* doc */
		return -ENODATA;
	}

	return 0;
}

/**
 * Read in the next header from the CUPS raster file
 * @param[in,out] ql Full job description
 * @retval number Header bytes read
 * @retval 0 on EOF, negative value on failure, 'errno' is valid in this case
 */
int cups_ql_raster_header_read(struct qldriver *ql)
{
	unsigned rc;

	rc = cupsRasterReadHeader2(ql->ras, &ql->raster_header);
	if (rc == 1)
		return (int)sizeof(struct cups_page_header2_s);

	return 0; /* rc == 0 means error and end of file m( */
}

/**
 * Convert Postscript Points (1/72 inch) to dots at the given resolution
 * @param[in] pts Postscript Points to convert
 * @param[in] resolution Dots Per Inch (DPI)
 * @return Dot count
 */
static unsigned convert_points_to_pixel(float pts, unsigned resolution)
{
	return (unsigned)(((pts * (float)resolution) / 72.0) + 0.5);
}

/**
 * Convert Postscript Points (1/72 inch) to millimeter
 * @param[in] pts Postscript Points to convert
 * @return Value in [mm]
 */
static float convert_points_to_mm(float pts)
{
	return (float)(pts * 25.4 / 72.0);
}

/**
 * @param[in] resolution Dots Per Inch (DPI)
 * @return Value in [mm]
 */
static float convert_pixel_to_mm(float pixel, unsigned resolution)
{
	return (float)((pixel / (float)resolution) * 25.4);
}

/**
 * Add a quirk to guess the size of the used medium based on the CUPS raster
 * @param[in,out] ql  Full job description
 *
 * While testing the driver and the CUPS framework, some strange things happened
 * in the printer chain. This quirk was a try to check if it is possible to print
 * from applications to a continuous length medium. Result: no it isn't.
 *
 * Refer @ref trouble_shooting for details.
 */
static __nonnull((1)) __unused void cups_ql_medium_size_quirk(struct qldriver *ql)
{
	/* Default medium size isn't known. Guess it from the raster */
	ql->trim.media_width_mm = convert_pixel_to_mm(ql->raster_header.cupsWidth, ql->raster_header.HWResolution[0]);
	ql->trim.media_height_mm = convert_pixel_to_mm(ql->raster_header.cupsHeight, ql->raster_header.HWResolution[1]);
	fprintf(stderr, "WARNING: Medium size guessed from raster: %.1f mm x %.1f m\n",
			ql->trim.media_width_mm, ql->trim.media_height_mm);

	ql->trim.media_width = ql->raster_header.cupsWidth;
	ql->trim.media_height = ql->raster_header.cupsHeight;
}

/**
 * @pre The CUPS raster header info must be valid, e.g. #cups_ql_raster_header_read() already called
 *
 * @note CUPS specific adaption
 */
int ql_raster_limits_get(struct qldriver *ql)
{
	const ppd_size_t *page_info;

	assert(ql->raster_header.HWResolution[0] == 300); /* horizontal resolution */
	assert(ql->raster_header.HWResolution[1] == 300); /* vertical resolution */

	page_info = ppdPageSize(ql->ppd, NULL); /* get info from the default media */
	if (page_info == NULL) {
#if USE_PRINT_QUIRKS
		fprintf(stderr, "DEBUG: PPD failed to report page size. Broken PPD? Guess it from the raster instead\n");
		cups_ql_medium_size_quirk(ql);
#else
		fprintf(stderr, "ERROR: PPD failed to report page size. Broken PPD? Cannot print\n");
		return -EINVAL;
#endif
	} else {
		/* Default medium size is defined and known */
		assert(page_info->marked != 0);

		fprintf(stderr, "DEBUG: PPD reported page size is: %.1f points x %.1f points\n", page_info->width, page_info->length);
		ql->trim.media_width_mm = convert_points_to_mm(page_info->width);
		ql->trim.media_height_mm = convert_points_to_mm(page_info->length);
		fprintf(stderr, "DEBUG: Medium size from PPD: %.1f mm x %.1f m\n", ql->trim.media_width_mm, ql->trim.media_height_mm);

		ql->trim.media_width = convert_points_to_pixel(page_info->width, ql->raster_header.HWResolution[0]);
		ql->trim.media_height = convert_points_to_pixel(page_info->length, ql->raster_header.HWResolution[1]);
	}

	ql->trim.raster_width = ql->raster_header.cupsWidth;
	ql->trim.raster_height = ql->raster_header.cupsHeight;

	/* TODO add vertical resolution info to support this mode */
	return 0;
}

/**
 * Convert a positive ASCII number ('\0' terminated) into a positive integer
 * @param[in] string The string toconvert
 * @retval Positive The converted ASCII string
 * @retval -EINVAL String has more characters than a signed int can fit
 * @retval -EINVAL @b string seems not contain an ASCII number
 * @retval -ERANGE ASCII number is larger than a signed int
 *
 * @note This is more or less a copy of caps_helper_number_convert()
 */
static __nonnull((1)) int number_convert(const char *string)
{
	unsigned long int number;
	char *endptr;

	/* assume the max integer has a length of 10 characters (UINT_MAX -> 4294967295) */
	if (strlen(string) > 11) {
		fprintf(stderr, "DEBUG: String number exceeds the expected length\n"); /* doc */
		return -EINVAL;
	}

	number = strtoul(string, &endptr, 0);
	if (number == 0 && string == endptr) {
		fprintf(stderr, "DEBUG: Numerical value expected - but found illegal value: '%s'\n", string); /* doc */
		return -EINVAL;
	}

	if (number == ULONG_MAX && errno == ERANGE) {
		fprintf(stderr, "DEBUG: Invalid '%s' number found\n", string); /* doc */
		return -ERANGE;
	}

	if (number > INT_MAX) {
		fprintf(stderr, "DEBUG: Out of signed integer range number '%s' found\n", string); /* doc */
		return -ERANGE;
	}

	/* range already checked: it is a positive *integer* */
	return (int)number;
}

/**
 * Generic function to read a value from the PPD
 * @param[in] ql Full job description
 * @param[in] group_name Attribute group name
 * @param[in] attribute_name Attribute name (can be NULL)
 * @param[in] default_val Default value if the group or the attribute wasn't found
 * @retval Value The value read from the PPD's @b group_name / @b attribute_name
 * @retval Default The @b default if the @b group_name / @b attribute_name wasn't found
 *
 * The PPD contains lines in the form:
@verbatim
  * <group name> <attribute name>: "<value>"
  * <group name>: "<value>"
@endverbatim
  * The latter one you can read the \<value\> with @b attribute_name set to NULL
  */
static __nonnull((1,2)) unsigned int attribute_group_value_get(struct qldriver *ql, const char *group_name, const char *attribute_name, unsigned int default_val)
{
	const ppd_attr_t *attr;
	int ret;

	attr = ppdFindAttr(ql->ppd, group_name, attribute_name);
	if (attr == NULL)
		return default_val;

	ret = number_convert(attr->value);
	if (ret < 0) {
		fprintf(stderr, "DEBUG: Value for %s/%s isn't a valid numerical value: '%s'\n", group_name, attribute_name, attr->value); /* doc */
		return default_val;
	}

	return (unsigned int)ret;
}

/**
 * Grap a choice from the PPD
 * @param[in] ql Full job description
 * @param[in] kw Keyword
 * @return The selected string
 */
static __nonnull((1,2)) const char *choice_value_get(struct qldriver *ql, const char *kw)
{
	ppd_attr_t *attr;
	ppd_choice_t *choice;

	attr = ppdFindAttr(ql->ppd, kw, NULL);
	if (attr == NULL) {
		choice = ppdFindMarkedChoice(ql->ppd, kw);
		if (choice == NULL)
			return NULL;
		return choice->choice;
	}

	return attr->value;
}

/**
 * Read in the bytes per line entry
 * @param[in] ql Full job description
 *
 * This value is required to "know" the amount of dots the printer's head is capable to print per
 * line. All known print heads are simply monochrome, so 8 dots fits into a byte. And it seems this
 * amount of bytes must always be sent, e.g. independendly of the used medium. Exception seems to be
 * when compression is used. But at 90 bytes (or 196 bytes) and a USB interface, compression makes no
 * sense. That's why some printers do not support compression at all.
 */
static __nonnull((1)) void bytes_per_line_init(struct qldriver *ql)
{
	ql->bytes_per_line = attribute_group_value_get(ql, "qlprinter", "bpl", DEFAULT_QL_LINE_SIZE);
	fprintf(stderr, "DEBUG: Running printer line size in bytes: %d\n", ql->bytes_per_line);
}

/**
 * Read in printer type
 * @param[in] ql Full job description
 *
 * This information is very important, since various printer variants interprete the same command
 * in a different manner. Some routines rely on this printer type number to do the right things.
 */
static __nonnull((1)) int printer_type_init(struct qldriver *ql)
{
	const ppd_attr_t *attr;

	/* We don't know, yet */
	ql->bi_colour_prn_caps = 0;

	ql->printer = (enum printer_type)attribute_group_value_get(ql, "cupsModelNumber", NULL, BROTHER_QL_UNKNOWN);
	if (ql->printer == BROTHER_QL_UNKNOWN) {
		fprintf(stderr, "DEBUG: Missing 'cupsModelNumber' attribute in PPD, e.g. undefined printer model number. Cannot continue.\n"); /* doc */
		return -ENODEV;
	}
	fprintf(stderr, "DEBUG: Running printer %d\n", ql->printer);

	attr = ppdFindAttr(ql->ppd, "ColorDevice", NULL);
	if (attr == NULL) {
		fprintf(stderr, "DEBUG: Missing 'ColorDevice' attribute in PPD. Continue as monochrome device!\n"); /* doc */
		return 0;
	}

	if (!strcasecmp(attr->value, "True")) {
		ql->bi_colour_prn_caps = 1; /* This printer can deal with bi-colour labels */
		fprintf(stderr, "DEBUG: Device is capable to print bi-colour\n");
	}

	return 0;
}

/**
 * Setup the halftone method for this job
 * @param[in] ql Full job description
 *
 * If no defintion is found, it falls back to #HT_NONE
 *
 * @note If the definition is bad/invalid, this function does not return.
 */
static __nonnull((1)) void halftone_type_init(struct qldriver *ql)
{
	const char *setting;

	setting = choice_value_get(ql, "Halftone");
	if (setting == NULL) {
		fprintf(stderr, "DEBUG: 'Halftone' setting not found in PPD. Fix it!\n"); /* doc */
		fprintf(stderr, "DEBUG: Falling back to 'binary dither', e.g. none at all\n");
		ql->half_tone_type = HT_NONE;
		return;
	}

	fprintf(stderr, "DEBUG: Halftone method set to '%s'\n", setting);

	if (!strcasecmp(setting, "BinaryDither"))
		ql->half_tone_type = HT_NONE;
	else if (!strcasecmp(setting, "OrderedDither"))
		ql->half_tone_type = HT_ORDERED;
	else if (!strcasecmp(setting, "ErrorDiffusion"))
		ql->half_tone_type = HT_ERROR_DIFFUSION;
	else {
		fprintf(stderr, "DEBUG: 'Halftone' with invalid setting found: '%s'. Fix your PPD\n", setting); /* doc */
		exit(1);
	}
}

/**
 * Read in the lower angle to detect a red dot from the PPD
 * @param[in] ql Full job description
 *
 * An angle above this value will be dropped
 *
 * @note Refer @ref bi_coloured_print for details
 */
static __nonnull((1)) void red_detection_lower_angle(struct qldriver *ql)
{
	ql->lower_red_angle = (double)attribute_group_value_get(ql, "qlprinter", "lower_red_angle", 30);
}

/**
 * Read in the higher angle to detect a red dot from the PPD
 * @param[in] ql Full job description
 *
 * An angle below this value will be dropped
 *
 * @note Refer @ref bi_coloured_print for details
 */
static __nonnull((1)) void red_detection_higher_angle(struct qldriver *ql)
{
	ql->upper_red_angle = (double)attribute_group_value_get(ql, "qlprinter", "higher_red_angle", 350);
}

/**
 * Read the lower saturation to detect a red dot from the PPD
 * @param[in] ql Full job description
 *
 * If the angle defines a red, a saturation below this value will be dropped.
 *
 * @note Refer @ref bi_coloured_print for details
 */
static __nonnull((1)) void red_detection_lower_saturation(struct qldriver *ql)
{
	ql->lower_red_sat = (double)attribute_group_value_get(ql, "qlprinter", "lower_red_saturation", 32);
}

/**
 * Read the lower value to detect a red dot from the PPD
 * @param[in] ql Full job description
 *
 * If the angle defines a red, a saturation below this value will be dropped.
 *
 * @note Refer @ref bi_coloured_print for details
 */
static __nonnull((1)) void red_detection_lower_value(struct qldriver *ql)
{
	ql->lower_red_val = (double)attribute_group_value_get(ql, "qlprinter", "lower_red_value", 32);
}

/**
 * Read in the required valued to detect red dots in a CUPS raster
 * @param[in] ql Full job description
 *
 * @note Refer @ref bi_coloured_print for details
 */
static __nonnull((1)) void red_detection_init(struct qldriver *ql)
{
	red_detection_lower_angle(ql);
	red_detection_higher_angle(ql);
	red_detection_lower_saturation(ql);
	red_detection_lower_value(ql);
}

/**
 * Read in the amount of leading zero bytes
 * @param[in] ql Full job description
 *
 * Various printer variants define a different amount of zero bytes leading a printing job.
 * QL500 defines 200 bytes, QL650 defines 350 bytes and a QL800 defines 400 bytes.
 */
static __nonnull((1)) void leading_bytes_setup(struct qldriver *ql)
{
	ql->leading_bytes = attribute_group_value_get(ql, "qlprinter", "leading_bytes", 400);
}

/**
 * The printers have some margin requirements at top and bottom of a label for continuous length label
 * @param[in] ql Full job description
 *
 * These defines only the min and max hardware margins. It is (theoretical) up to the user, to define
 * these margins with each job. It is some kind of micro optimization, because it safes bytes to be
 * sent to the printer if the marging should be larger than the minimum of 3 mm. But I think it is
 * more relevant to make use of this hardware feature for (slow) serial interfaces, than for USB.
 *
 * So, I think it is easier to setup a specific margin at the top and bottom of the document which
 * extends the 3mm hardware margins by the cost of sending empty lines to the printer to keep these
 * lines empty. Advantage of this approach is both margins can be different. With the hardware solution
 * the top and bottom margins are always the same.
 *
 * @todo Integrate the 3 mm top/bottom margins into the continuous length paper definition?
 */
static __nonnull((1)) void margin_setup(struct qldriver *ql)
{
	ql->min_margin = (unsigned)attribute_group_value_get(ql, "qlprinter", "mininmal_margin", 3);
	ql->max_margin = (unsigned)attribute_group_value_get(ql, "qlprinter", "maximal_margin", 127);
}

/**
 * Setup the printer's high resolution capabilities
 * @param[in] ql Full job description
 *
 * @todo This function still needs to be implemented
 */
static __nonnull((1)) void high_resoultion_setup(struct qldriver *ql)
{
	ql->high_density_caps = 0; /* TODO Read in from the PPD (printer capability) */
	ql->high_density_print = 0; /* TODO Read in from the PPD (resolution setting) */
}

/**
 * Setup the printer's auto cutter capability
 * @param[in] ql Full job description
 */
static __nonnull((1)) void auto_cutter_setup(struct qldriver *ql)
{
	const ppd_attr_t *attr;

	ql->auto_cutter_caps = 0; /* default is always: No! */

	attr = ppdFindAttr(ql->ppd, "qlprinter", "cutter-type");
	if (attr == NULL)
		return;

	if (!strcasecmp(attr->value, "auto"))
		ql->auto_cutter_caps = 1;
}

/**
 * Setup the printer's power off feature according to user settings
 * @param[in] ql Full job description
 *
 * The power off feature is a little bit anoying. After the programmable
 * idle timout it disconnects from the USB and powers itself off. You can't
 * wake it again remotely.
 *
 * @note Only a few of the QL printers seems to support this feature.
 *       At least the QL500 doesn't and the QL800 does.
 */
static __nonnull((1)) void auto_power_off_setup(struct qldriver *ql)
{
	unsigned short timout_setting;
	const char *setting;
	int rc;

	setting = choice_value_get(ql, "PowerOFF");
	if (setting == NULL)
		return; /* This printer doesn't support this feature */

	if (!strcasecmp(setting, "Unchanged"))
		return; /* Nothing to be done here */

	if (!strcasecmp(setting, "MIN10"))
		timout_setting = 0x0001;
	else if (!strcasecmp(setting, "MIN20"))
		timout_setting = 0x0002;
	else if (!strcasecmp(setting, "MIN30"))
		timout_setting = 0x0003;
	else if (!strcasecmp(setting, "MIN40"))
		timout_setting = 0x0004;
	else if (!strcasecmp(setting, "MIN50"))
		timout_setting = 0x0005;
	else if (!strcasecmp(setting, "MIN60"))
		timout_setting = 0x0006;
	else if (!strcasecmp(setting, "Disabled"))
		timout_setting = 0x0000;
	else {
		fprintf(stderr, "WARNING: Skipping invalid power off feature '%s'. Fix your PPD.\n", setting);
		return;
	}

	rc = ql_auto_powerdown(ql, timout_setting);
	if (rc < 0)
		fprintf(stderr, "WARNING: Failed to setup the power off feature. Continue without.\n");
}

/**
 * Setup the printer's power on feature according to user settings
 * @param[in] ql Full job description
 *
 * If the printer is connected to an external power supply, you can select, if it should
 * switch on immediately, or should wait for manual touching the power button to swich on.
 */
static __nonnull((1)) void auto_power_on_setup(struct qldriver *ql)
{
	unsigned short pwron_setting;
	const char *setting;
	int rc;

	setting = choice_value_get(ql, "PowerOn");
	if (setting == NULL)
		return; /* This printer doesn't support this feature */

	if (!strcasecmp(setting, "Unchanged"))
		return; /* Nothing to be done here */

	if (!strcasecmp(setting, "ON"))
		pwron_setting = 0x0001;
	else if (!strcasecmp(setting, "OFF"))
		pwron_setting = 0x0000;
	else {
		fprintf(stderr, "WARNING: Skipping invalid power on feature '%s'. Fix your PPD.\n", setting);
		return;
	}

	rc = ql_auto_powerup(ql, pwron_setting);
	if (rc < 0)
		fprintf(stderr, "WARNING: Failed to setup the power on feature. Continue without.\n");
}

/**
 * Init required information about the printer device
 * @param[in] ql Full job description
 * @retval 0 On success (currently always)
 */
int cups_ql_driver_init(struct qldriver *ql)
{
	int rc;

	rc = printer_type_init(ql);
	if (rc < 0)
		return rc;

	bytes_per_line_init(ql);

	halftone_type_init(ql);

	red_detection_init(ql);

	leading_bytes_setup(ql);

	margin_setup(ql);

	high_resoultion_setup(ql);

	auto_cutter_setup(ql);

	auto_power_off_setup(ql);

	auto_power_on_setup(ql);

	return 0;
}

/**
 * Get the raster's colour space and check if it can be printed on this printer and medium
 * @param[in] ql Full job description
 * @retval 0 On success
 * @retval -EINVAL Unsupported colour format (due to wrong medium or wrong device)
 *
 * @note Must be called on every single page (e.g. on each raster header read)
 * @pre A raster header must already be read.
 */
static __nonnull((1)) int cups_ql_job_check_colour(struct qldriver *ql)
{
	if (ql->raster_header.cupsColorSpace == CUPS_CSPACE_W) {
		fprintf(stderr, "DEBUG: Monochrome raster detected\n");
		if (ql->raster_header.cupsBitsPerColor != 8) {
			fprintf(stderr, "DEBUG: Input raster expected as 8 bit grey format, but it is: %d bit. Cannot continue\n", ql->raster_header.cupsBitsPerColor); /* doc */
			return -EINVAL;
		}
		ql->coloured_raster = 0;
		return 0; /* We support this raster format */
	}

	if (ql->raster_header.cupsColorSpace == CUPS_CSPACE_RGB) {
		fprintf(stderr, "DEBUG: Coloured raster detected\n");
		if (!ql->bi_colour_prn_caps) {
			fprintf(stderr, "DEBUG: Printer cannot print in bi-colour mode\n"); /* doc */
			return -EINVAL;
		}

		if (!ql->bi_colour_medium_caps) {
			fprintf(stderr, "DEBUG: Request to print in bi-colour mode, but the medium isn't capable doing so.\n"); /* doc */
			return -EINVAL;
		}

		ql->coloured_raster = 1;
		return 0; /* We support this raster format */
	}

	fprintf(stderr, "DEBUG: Unsupported raster colour format detected. Check your printer's setup.\n"); /* doc */
	return -EINVAL;
}

/**
 * Guess the medium parameters.
 * @param[in] ql Full job description
 * @retval 0 On success
 *
 * In the absence of a DefaultPageSize (to make the other stages work as expected)
 * we still need some information about the medium. Guess it instead based on the
 * given CUPS raster. Hopefully the user knows what he does. Famous last words...
 *
 * In this case we:
 *  - expect a continuous length medium
 *  - we expect a bi-colour medium, if the raster is coloured
 *  - left margin set to the one for full width medium
 *
 * @pre The header of the CUPS raster must be valid
 */
static __nonnull((1)) __unused int cups_ql_medium_guess(struct qldriver *ql)
{
	ql->pre_sized = 0;
	fprintf(stderr, "DEBUG: Assume continuous length medium\n");

	if (ql->raster_header.cupsColorSpace == CUPS_CSPACE_RGB) {
		ql->bi_colour_medium_caps = 1;
		fprintf(stderr, "DEBUG: Assume bi-colour medium, due to coloured input raster\n");
	} else {
		ql->bi_colour_medium_caps = 0;
		fprintf(stderr, "DEBUG: Assume monochrome medium\n");
	}

	ql->left_hardware_margin = 2.0;
	fprintf(stderr, "DEBUG: Assume %.2f mm as its left margin\n", ql->left_hardware_margin);

	return 0;
}

/**
 * Update the media dependent base settings according to the PPD information
 * @param[in] ql Full job description
 * @retval 0 On success
 * @retval -EINVAL Unsupported DK roll
 *
 * @attention The @b media_height value can be very large for continuous length tapes!
 * @note Can be called prior reading the first raster header
 *
 * @todo Can be called in #cups_ql_driver_init() as well
 */
static __nonnull((1)) int cups_ql_medium_detect(struct qldriver *ql)
{
	struct dk_roll_types medium;
	ppd_size_t *page_info;
	int rc;

	/* We don't know, yet */
	ql->pre_sized = 0;
	ql->bi_colour_medium_caps = 0;

	page_info = ppdPageSize(ql->ppd, NULL); /* Get info from the default medium */
	if (page_info == NULL) {
#if USE_PRINT_QUIRKS
		fprintf(stderr, "WARNING: No default medium set. Trying to continue without and guess its features\n");
		return cups_ql_medium_guess(ql);
#else
		fprintf(stderr, "ERROR: No default medium set. PPD content broken? Unable to print\n");
		return -EINVAL;
#endif
	}

	assert(page_info->name != NULL);
	fprintf(stderr, "DEBUG: Use settings from roll '%s'\n", page_info->name);

	rc = ql_dk_information_get(&medium, page_info->name);
	if (rc < 0) {
		fprintf(stderr, "DEBUG: Missing internal definition for roll type '%s'. Expect the print will fail somehow.\n", page_info->name); /* doc */
		return 0;
	}

	if (medium.caps & DK_CAP_DIE_CUT) {
		fprintf(stderr, "DEBUG: Pre-sized label medium detected.\n");
		ql->pre_sized = 1;
	}

	if (medium.caps & DK_CAP_CONTINUOUS) {
		fprintf(stderr, "DEBUG: Continuous length label medium detected.\n");
		/* no need to change anything, it is the default */
	}

	if (medium.caps & DK_CAP_RED) {
		fprintf(stderr, "DEBUG: Bi-colour capable medium detected.\n");
		ql->bi_colour_medium_caps = 1;
	}

	ql->left_hardware_margin = medium.margin;
	fprintf(stderr, "DEBUG: Left hardware margin: %.1f mm.\n", ql->left_hardware_margin);

	return 0;
}

/**
 * Warn about an annoying behaviour of @c pdftopdf and @c pdftoraster
 * @param[in] ql Full job description
 *
 * @attention @c pdftopdf tends to apply annoying rotations to your document. If your document is
 *            oriented in landscape and would fit to a continuous length label, @c pdftopdf will
 *            rotate your document, because the continuous length label looks like a portrait oriented
 *            label to it (for example DK2215: 62 mm width and 1000 mm length). Using the
 *            @c pdfAutoRotate=off print parameter solves this issue.
 *
 * @attention @c pdftoraster tends to ignore the @c PageSize printing parameter and creates an
 *            inappropriate raster instead. For example, if your document is a 62 mm x 29 mm landscape
 *            oriented label and you want to print it on a continuous length label like the DK2251
 *            roll is, it prefers the DK1209 portrait label roll instead. And creates the raster for
 *            it, which isn't what you expect.
 */
static __nonnull((1)) void cups_ql_input_raster_warn(struct qldriver *ql)
{
	static const int name_length = sizeof(ql->raster_header.cupsPageSizeName);
	const ppd_size_t *page_info;
	/*
	 * We are interested in the medium name the user is interested in:
	 * the default PageSize he sets up with the PageSize=<name> printing parameter.
	 * It should be the same, the raster is made for.
	 */
	page_info = ppdPageSize(ql->ppd, NULL);
	if (page_info == NULL) {
		fprintf(stderr, "DEBUG: Raster is made for '%.*s' medium\n", name_length, ql->raster_header.cupsPageSizeName);
		return;
	}

	assert(page_info->marked != 0);
	if (!strncmp(page_info->name, ql->raster_header.cupsPageSizeName, (size_t)name_length)) {
		/* Source and destinaten matches */
		fprintf(stderr, "DEBUG: Raster is made for '%.*s' medium\n", name_length, ql->raster_header.cupsPageSizeName);
		return;
	}

	/* Warn, because pdftoraster has done a bad job */
	fprintf(stderr, "WARNING: 'pdftoraster' has guessed a bad page format. Your print will fail somehow\n"); // doc
	fprintf(stderr, "WARNING: requested print page is: '%s', but the CUPS raster is made for: '%.*s'\n", // doc
			page_info->name, name_length, ql->raster_header.cupsPageSizeName);
}

/**
 * Basic check if the given raster file matches the printer
 * @param[in] ql Full job description
 * @retval 0 On success
 *
 * All these @ref annoying_warning_messages should'n occur. If they do, the used PPD is broken.
 *
 * @pre The raster header is valid
 *
 * For the records (Infos from some "spec-ppd.html" web site):
 *
 * - AdvanceDistance: Specifies the number of points to advance roll media after printing.
 * - AdvanceMedia: Specifies when to advance the media: 0 = never, 1 = after the file, 2 = after the job, 3 = after the set, and 4 = after the page.
 * - CutMedia: Specifies when to cut the media: 0 = never, 1 = after the file, 2 = after the job, 3 = after the set, and 4 = after the page.
 *
 */
static __nonnull((1)) int cups_ql_input_raster_check(struct qldriver *ql)
{
	cups_ql_input_raster_warn(ql); /* m( */

	if (ql->raster_header.Duplex == CUPS_TRUE)
		fprintf(stderr, "DEBUG: Duplex flag ignored\n"); /* doc */
	if (ql->raster_header.Collate == CUPS_TRUE)
		fprintf(stderr, "DEBUG: Collate flag ignored\n"); /* doc */
	if (ql->raster_header.Jog != CUPS_JOG_NONE)
		fprintf(stderr, "DEBUG: Jog (%d) flag ignored\n", ql->raster_header.Jog); /* doc */
	if (ql->raster_header.ManualFeed == CUPS_TRUE)
		fprintf(stderr, "DEBUG: ManualFeed flag ignored\n"); /* doc */
	if (ql->raster_header.MirrorPrint == CUPS_TRUE)
		fprintf(stderr, "DEBUG: MirrorPrint flag ignored\n"); /* doc */
	if (ql->raster_header.NegativePrint == CUPS_TRUE)
		fprintf(stderr, "DEBUG: NegativePrint flag ignored\n"); /* doc */
	if (ql->raster_header.NumCopies > 1)
		fprintf(stderr, "DEBUG: NumCopies (%u) ignored\n", ql->raster_header.NumCopies); /* doc */
	if (ql->raster_header.Orientation != CUPS_ORIENT_0)
		fprintf(stderr, "DEBUG: Orientation (%d) flag ignored\n", ql->raster_header.Orientation); /* doc */
	if (ql->raster_header.OutputFaceUp == CUPS_TRUE)
		fprintf(stderr, "DEBUG: OutputFaceUp flag ignored\n"); /* doc */
	if (ql->raster_header.Separations == CUPS_TRUE)
		fprintf(stderr, "DEBUG: Separations flag ignored\n"); /* doc */
	if (ql->raster_header.TraySwitch == CUPS_TRUE)
		fprintf(stderr, "DEBUG: TraySwitch flag ignored\n"); /* doc */
	if (ql->raster_header.Tumble == CUPS_TRUE)
		fprintf(stderr, "DEBUG: Tumble flag ignored\n"); /* doc */
	if (ql->raster_header.CutMedia != CUPS_CUT_NONE)
		fprintf(stderr, "DEBUG: CutMedia (%d) flag ignored, yet\n", ql->raster_header.CutMedia); /* doc */
	if (ql->raster_header.LeadingEdge != CUPS_EDGE_TOP)
		fprintf(stderr, "DEBUG: LeadingEdge (%d) flag ignored, yet\n", ql->raster_header.LeadingEdge); /* doc */
	if (ql->raster_header.AdvanceDistance != 0)
		fprintf(stderr, "DEBUG: AdvanceDistance (%u) value ignored, yet\n", ql->raster_header.AdvanceDistance); /* doc */
	if (ql->raster_header.AdvanceMedia != CUPS_ADVANCE_NONE)
		fprintf(stderr, "DEBUG: AdvanceMedia (%d) flag ignored, yet\n", ql->raster_header.AdvanceMedia); /* doc */
	if (ql->raster_header.cupsInteger[0] != 0)
		fprintf(stderr, "DEBUG: cupsInteger0 (%d) value ignored, yet\n", ql->raster_header.cupsInteger[0]);

	fprintf(stderr, "DEBUG: Intended print size: %.1f mm (%u pts) x %.1f mm (%u pts)\n",
			convert_points_to_mm((float)ql->raster_header.PageSize[0]),
			ql->raster_header.PageSize[0],
			convert_points_to_mm((float)ql->raster_header.PageSize[1]),
			ql->raster_header.PageSize[1]);

	return 0;
}

/**
 * Skip some amount of raster lines
 * @param[in] ql Full job description
 * @param[in] lines How many lines to skip
 * @param[in] cnt Count of bytes per line
 * @retval 0 On success
 * @retval -ENODATA Mature end of CUPS raster input data
 */
static __nonnull((1)) int cups_ql_raster_lines_skip(struct qldriver *ql, unsigned lines, size_t cnt)
{
	unsigned char skip_buffer[cnt];
	int rc;

	while (lines > 0) {
		rc = cups_ql_next_line_read(ql, cnt, skip_buffer);
		if (rc < 0)
			return rc;
		lines--;
	}

	return 0;
}

/**
 * CUPS specific document print routine
 * @param[in] ql Full job description
 * @retval 0 On success
 * @retval -EINVAL Unsupported raster input format
 *
 * This function prepares the print. E.g. configure the printer according to the given CUPS raster
 * format.
 *
 * We need to check the raster header in order to have an idea, what has to be printed (monochrome
 * versus bi-colour). We can check the medium size versus the raster size here to be able to print
 * something.
 *
 * The routine loops through all pages inside the CUPS raster file and calls the corresponding
 * encoding routine to do the job.
 *
 * @note On a termination request we rely on the specific encoding function (#cups_ql_colour_page_print()
 *       or #cups_ql_monochome_page_print()) to return with @c -ECANCELED, because the routine itself knows
 *       how to terminate the print at the printer's side as well
 *
 * @pre All pages in the raster file use the same colour format (e.g. no RGB/grey mix)
 */
int cups_ql_job_print(struct qldriver *ql)
{
	int first_loop = 1;
	int rc = -EINVAL;

	assert(ql->half_tone_type == HT_ERROR_DIFFUSION || ql->half_tone_type == HT_ORDERED || ql->half_tone_type == HT_NONE);


	/* One loop per raster header */
	while (cups_ql_raster_header_read(ql) != 0) {
		if (first_loop) {
			/* Detect some basic information about the medium once per job */
			rc = cups_ql_medium_detect(ql);
			if (rc < 0)
				return rc;
		}

		/* Check if we can print this page */
		rc = cups_ql_input_raster_check(ql);
		if (rc < 0)
			break;

		if (first_loop) {
			/* We need to detect the raster's colour format once */
			rc = cups_ql_job_check_colour(ql);
			if (rc < 0)
				break;
		}

		/* Trim the input raster according to the available medium and capabilities */
		rc = ql_input_raster_crop(ql);
		if (rc < 0)
			break;

		/* If we have to skip some lines, consume them here at top */
		if (ql->trim.top_skip > 0) {
			fprintf(stderr, "DEBUG: skipping %u lines at top\n", ql->trim.top_skip);
			rc = cups_ql_raster_lines_skip(ql, ql->trim.top_skip, ql->raster_header.cupsBytesPerLine);
			if (rc < 0)
				goto on_data_error; /* Terminate the job. At this point the printer has *no* job, or the previous page is *complete* */
		}

		/* Print exactly this page, e.g. the remaining lines */
		if (ql->coloured_raster)
			rc = cups_ql_colour_page_print(ql, first_loop);
		else
			rc = cups_ql_monochome_page_print(ql, first_loop);

		first_loop = 0;

		if (rc != 0)
			break;

		/* If we have to skip some lines, consume them here at bottom */
		if (ql->trim.top_skip > 0) {
			fprintf(stderr, "DEBUG: skipping %u lines at bottom\n", ql->trim.top_skip);
			rc = cups_ql_raster_lines_skip(ql, ql->trim.top_skip, ql->raster_header.cupsBytesPerLine);
			if (rc < 0)
				goto on_data_error; /* Terminate the job. At this point the printer has a *complete* page */
		}
	}

	if (terminate == 0 && rc == 0)
		rc = ql_job_footer_generate(ql); /* Finish job */

	return rc;

on_data_error:
	if (!first_loop) {
		if (ql_job_footer_generate(ql) < 0) /* Terminate job, but keep the original error value TODO This finishes the job instead of terminating it! */
			fprintf(stderr, "DEBUG: failed to finish the current job. Please restart your printer\n");
	}

	return rc;
}

/**
 * State/error/warning/info reporting function
 * @param[in] level Verbosity level of this message
 * @param[in] scope The message's scope, like it's package name
 * @param[in] fn Name of the function, this output is from
 * @param[in] ln Line number this call is from
 * @param[in] format The format string. Refer the printf() manual for further details
 *
 * @note Do not use this function directly. You should always use the convenience
 * macros @b caps_print_* instead.
 */
void cups_message_print(const char *level, const char *scope, const char *fn, int ln, const char *format, ...)
{
	va_list args;

	if (level == NULL)
		level = "DEBUG2";

	va_start(args, format);

	fprintf(stderr, "%s: (%s:%s:%d) ", level, scope, fn, ln);

	vfprintf(stderr, format, args);

	va_end(args);
}

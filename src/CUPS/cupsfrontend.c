/*
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * (c) 2021 Juergen Borleis <projects@caps-printing.org>
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 */
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>

#include <cups/cups.h>
#include <cups/ppd.h>
#include <cups/raster.h>

#include "qldriver.h"

/**
 * @file cupsfrontend.c
 * @brief Functions to act as a CUPS filter
 * @copyright GNU General Public License 2.0 or later
 * @author Jürgen Borleis
 */

/** Change to '1' to signal the program should stop */
int terminate;

static struct sigaction signal_pipe[2] = {
	{
		.sa_handler = SIG_IGN,	/* should be ignored in all cases */
		.sa_flags = 0,
	},
};

static void handle_sigterm(int signal __unused)
{
	terminate = 1;
}

static struct sigaction signal_term[2] = {
	{
		.sa_handler = handle_sigterm,
		.sa_flags = 0,
	},
};

/**
 * Prepare for signal handling
 * @retval 0 On success
 * @retval Negative on failure
 *
 * Signals handled are:
 * - SIGTERM: when a printing job is canceled or held
 * - SIGPIPE: when an upstream or downstream filter/backend exits with a non-zero status
 *
 * Note: SIGPIPE should be ignored in any case in this environment.
 */
static int prepare_signals(void)
{
	int rc;

	rc = sigaction(SIGPIPE, &signal_pipe[0], &signal_pipe[1]);
	if (rc != 0) {
		fprintf(stderr, "DEBUG: Establishing signal handler for SIGPIPE: %m");
		return rc;
	}

	rc = sigaction(SIGTERM, &signal_term[0], &signal_term[1]);
	if (rc != 0) {
		fprintf(stderr, "DEBUG: Establishing signal handler for SIGTERM: %m");
		return rc;
	}

	return 0;
}

/**
 * 'Take a look' into the CUPS raster file
 * @param[in,out] ql Full job description
 * @retval 0 On succes
 * @retval -EINVAL Failed to open the raster input file
 */
static int cups_ql_input_raster_file_open(struct qldriver *ql)
{
	/* Open the CUPS raster data stream to read in */
	ql->ras = cupsRasterOpen(ql->fd, CUPS_RASTER_READ);
	if (ql->ras == NULL)
		return -EINVAL;

	return 0;
}

/**
 * Get access to the printer's PPD file and its settings for this job
 * @param[in] ql Full job description
 * @param[in] option_cnt Count of elements in @b options
 * @param[in] options Optional parameters
 * @retval -EINVAL Missing PPD environment variable
 * @retval -ENODATA Bad PPD content
 * @retval 0 On success
 */
static int cups_ql_ppd_file_open(struct qldriver *ql, int option_cnt, cups_option_t options[option_cnt])
{
	const char *ppd_name;

	ppd_name = getenv("PPD");
	if (ppd_name == NULL) {
		fprintf(stderr, "DEBUG: PPD file not defined. Cannot continue"); /* doc */
		return -EINVAL;
	}

	ql->ppd = ppdOpenFile(ppd_name);
	if (ql->ppd == NULL) {
		ppd_status_t status;
		int linenum;
		fprintf(stderr, "DEBUG: The PPD file '%s' could not be opened", ppd_name); /* doc */
		status = ppdLastError(&linenum);
		fprintf(stderr, "DEBUG: %s on line %d.", ppdErrorString(status), linenum);
		return -ENODATA;
	}

	/* Adapt PPD's default settings to the printing parameter parameters (if any) */
	ppdMarkDefaults(ql->ppd);
	cupsMarkOptions(ql->ppd, option_cnt, options);

	return 0;
}

/**
 * Main entry function.
 * @param argc  number of command line arguments plus one
 * @param argv  command line arguments
 * @return      0 if success, nonzero otherwise
 *
 * https://opensource.apple.com/source/cups/cups-327/cups/doc/help/api-filter.html
 *
 * We get:
 * - argv[1] The job ID
 * - argv[2] The user printing the job
 * - argv[3] The job name/title
 * - argv[4] The number of copies to print (only valid if argv[6] is given!)
 * - argv[5] The options that were provided when the job was submitted
 * - argv[6] The file to print (optional)
 */
int main(int argc, const char *argv[])
{
	int rc, ret = EXIT_FAILURE, num_options;
	cups_option_t *options;
	struct qldriver ql = { .bytes_per_line = 0, };

	if (argc < 6 || argc > 7) {
		fprintf(stderr, "Usage: %s job-id user title copies options [file]\n", argv[0]);
		return EXIT_FAILURE;
	}

	/* Make the printing parameter available */
	num_options = cupsParseOptions(argv[5], 0, &options);

	rc = cups_ql_ppd_file_open(&ql, num_options, options);
	if (rc < 0)
		goto on_error;

	/* We need our raster input */
	if (argc == 7) {
		ql.fd = open(argv[6], O_RDONLY);
		if (ql.fd < 0) {
			fprintf(stderr, "DEBUG: Failed to open raster input data file: %m\n"); /* doc */
			goto on_error;
		}
	} else {
		ql.fd = STDIN_FILENO;
	}

	rc = cups_ql_driver_init(&ql);
	if (rc < 0)
		goto on_error;

	rc = cups_ql_input_raster_file_open(&ql);
	if (rc < 0) {
		fprintf(stderr, "DEBUG: Failed to open raster input data\n"); /* doc */
		goto on_error;
	}

	prepare_signals();
	srandom((unsigned int)getpid()); /* we need some pseudo random for half tone generation */

	rc = cups_ql_job_print(&ql);
	if (rc == 0)
		ret = EXIT_SUCCESS;

	cupsRasterClose(ql.ras);

on_error:
	cupsFreeOptions(num_options, options);

	if (ql.fd > STDIN_FILENO)
		close(ql.fd);

	return ret;
}

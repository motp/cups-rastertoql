#pragma once
/*
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * (c) 2021 Juergen Borleis <projects@caps-printing.org>
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 */

/**
 * @file internal.h
 * @brief Some useful helpers for the programmers and the compiler
 */
#ifndef ARRAY_SIZE
# define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))
#endif

#if __has_attribute (packed)
# define __packed __attribute__((packed))
#else
# define __packed
#endif

#if __has_attribute (unused)
# define __unused __attribute__((unused))
#else
# define __unused
#endif

#if __has_attribute (nonstring)
# define __nonstring __attribute__((nonstring))
#else
# define __nonstring
#endif

/** Disable NLS for CUPS */
#define _(x) x

void cups_message_print(const char *level, const char *scope, const char *fn, int ln, const char *format, ...);

/** Output a panic message. After this message, the program should terminate */
#define caps_print_panic(fmt, ...) cups_message_print("DEBUG", PACKAGE, __func__, __LINE__, fmt, ##__VA_ARGS__)
/** Output an error message. After this message, the program may continue */
#define caps_print_error(fmt, ...) cups_message_print("DEBUG", PACKAGE, __func__, __LINE__, fmt, ##__VA_ARGS__)
/** Output a warning message. After this message, the program continues at best it can */
#define caps_print_warn(fmt, ...) cups_message_print("DEBUG", PACKAGE, __func__, __LINE__, fmt, ##__VA_ARGS__)
/** Output an informational message. Its just for user's information. Do not use this excessive! */
#define caps_print_info(fmt, ...) cups_message_print("DEBUG", PACKAGE, __func__, __LINE__, fmt, ##__VA_ARGS__)
/** Output a major trace message. Used to track major program states */
#define caps_print_loud(fmt, ...) cups_message_print("DEBUG", PACKAGE, __func__, __LINE__, fmt, ##__VA_ARGS__)
/** Output a minor trace message. Used to track program states in more detail */
#define caps_print_noisy(fmt, ...) cups_message_print("DEBUG2", PACKAGE, __func__, __LINE__, fmt, ##__VA_ARGS__)
/** Output of debug messages. Used to track program states in much more detail */
#define caps_print_debug(fmt, ...) cups_message_print("DEBUG2", PACKAGE, __func__, __LINE__, fmt, ##__VA_ARGS__)

/*
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * (c) 2021 Juergen Borleis <projects@caps-printing.org>
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 */
#include <assert.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <math.h>

#include "qldriver.h"

/**
 * @file qlcommon.c
 * @brief Common routines to deal with QL label printers
 * @copyright GNU General Public License 2.0 or later
 * @author Jürgen Borleis
 */
/**
 * Just a helper to write more readable code
 */
#define BIT(x) (1 << (x))

/**
 * Convert an array of signed shorts into a monochrome pattern (one signed short into one bit)
 * @param[in] input_cnt Count of pixel in @b input
 * @param[in] input Where to get the @b quantized input data
 * @param[out] data Where to store the converted output data
 *
 * @note Since the printer prints from right to left, each pattern gets mirrored(!) here.
 *       First pixel printed (at the right border) is the MSB of the first byte
 * @pre #PRINT_DOT_VAL means: print a dot, everything else means: do not print a dot,
 *      e.g. the input data must already be quantized
 */
void transform_to_monochrome(size_t input_cnt, const signed short input[input_cnt], unsigned char *data)
{
	unsigned char wire_data;
	size_t u, x;

	assert((input_cnt % 8) == 0);

	/* Hier fange ich mit dem rechten Rand vom Raster an und schreibe den linken Rand der Druckdaten
	 * Ergebnis:
	 * - ich sehe nichts auf dem Papier
	 * - auf dem 62 mm Papier (wo es egal ist, wo ich drucke) ist es spiegelrichtig
	 * - auf jeden Fall befindet sich im ptexplain-Programm das Bild am rechten Rand und ist damit
	 *   außerhalb des Papiers
	 * Was tue ich hier?
	 * - ich spiegele das Bild
	 * - drehe es aber auf die Unterseite
	 * - das Bild wird dort von rechts nach links gedruckt, von mir aus von oben gesehen von rechts nach links.
	 * -> deshalb muss es gespiegelt werden
	 *
	 * Hä? Das müsste doch aber auch mit kleineren Grafiken gehen?????
	 * - die kleinere Grafik fängt links an
	 * - auch sie wird nach rechts gespiegelt
	 * - und dann von unten gedruckt
	 * -> damit ist der linke Teil der Druckgrafik leer, denn der rechte Teil des Rasters war ja leer!
	 *
	 * Was muss ich tun?
	 *                     L                                   R
	 * ich habe:     Bits  1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10  <-- das wäre ein volles 62 mm Raster
	 * Ich brauche:  Bits 10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1
	 *
	 * ich habe:     Bits  1 | 2 | 3 | 4 | 5                       <-- das wäre ein 31 mm Raster
	 * Ich brauche:  Bits  5 | 4 | 3 | 2 | 1
	 */
	for (x = 0, u = input_cnt; u >= 8; x++, u -= 8) {
		/* Convert 8 consecutive signed shorts into one byte */
		wire_data = 0;
		if (input[u - 1] == PRINT_DOT_VAL)
			wire_data |= BIT(7);
		if (input[u - 2] == PRINT_DOT_VAL)
			wire_data |= BIT(6);
		if (input[u - 3] == PRINT_DOT_VAL)
			wire_data |= BIT(5);
		if (input[u - 4] == PRINT_DOT_VAL)
			wire_data |= BIT(4);
		if (input[u - 5] == PRINT_DOT_VAL)
			wire_data |= BIT(3);
		if (input[u - 6] == PRINT_DOT_VAL)
			wire_data |= BIT(2);
		if (input[u - 7] == PRINT_DOT_VAL)
			wire_data |= BIT(1);
		if (input[u - 8] == PRINT_DOT_VAL)
			wire_data |= BIT(0);
		data[x] = wire_data;
	}
}

/**
 * Finish the current page
 * @param[in] ql Full job description
 * @retval 0 On success
 * @retval Negative Error code from the #ql_printer_data_send() call
 *
 * This code is to be sent after a page.
 *
 * It seems, this command is only to be sent, if more than one page in one job should be printed.
 * If the job contains only one page, the #ql_last_page_print() is enough to do the right thing.
 *
 * @note To be sent at the end of a page's raster, if it isn't the last page to be print (according to the manual)
 */
static  __nonnull((1)) __wur int ql_page_print(struct qldriver *ql)
{
	static const unsigned char page = 0x0c; /* aka Form Feed (FF) */

	return ql_printer_data_send(ql, &page, sizeof(page), 0);
}

/**
 * Finish the current print job
 * @param[in] ql Full job description
 * @retval 0 On success
 * @retval Negative Error code from the #ql_printer_data_send() call
 *
 * This code is to be sent after the last page to finish the whole print job.
 *
 * @note To be sent at the end of a page's raster, if it is the last page of the job (according to the manual)
 */
static __nonnull((1)) __wur int ql_last_page_print(struct qldriver *ql)
{
	static const unsigned char last_page = 0x1a; /* aka Control-Z */

	return ql_printer_data_send(ql, &last_page, sizeof(last_page), 1); /* This time flush the data */
}

/**
 * Value to signal the printer, the print data will be sent without any compression.
 * Used in the #ql_compression_setup() function as its @b mode parameter
 */
#define QL_NO_COMPRESSION 0x00
/**
 * Value to signal the printer, the print data will be sent in TIFF compression.
 * Used in the #ql_compression_setup() function as its @b mode parameter
 * @attention Only for reference. Compression isn't supported, yet
 */
#define QL_TIFF_COMPRESSION 0x02

/**
 * Setup the line data compression mode
 * @param[in] ql Full job description
 * @param[in] mode Compression more, one of #QL_NO_COMPRESSION or #QL_TIFF_COMPRESSION
 * @retval 0 On success
 * @retval Negative Error code from the #ql_printer_data_send() call
 *
 * @note Only a few printers can deal with compression, so #QL_TIFF_COMPRESSION isn't supported, yet.
 *
 * @note To be sent at the beginning of a page's raster and required at each page (according to the manual)
 */
static __nonnull((1)) __wur int ql_compression_setup(struct qldriver *ql, unsigned mode)
{
	unsigned char comp_mode[2] = { 'M', };

	assert(mode == QL_NO_COMPRESSION);

	comp_mode[1] = (unsigned char)mode;
	return ql_printer_data_send(ql, &comp_mode, sizeof(comp_mode), 0);
}

/**
 * Set the printer's auto power down behaviour if it is idle
 * @param[in] ql Full job description
 * @param[in] time_out Time out when idle to enter power down mode
 *
 * @b time_out can be:
 * - 0x0000: disable auto power off
 * - 0x0001: auto power off after 10 minutes
 * - 0x0002: auto power off after 20 minutes
 * - 0x0003: auto power off after 30 minutes
 * - 0x0004: auto power off after 40 minutes
 * - 0x0005: auto power off after 50 minutes
 * - 0x0006: auto power off after 60 minutes
 *
 * If the printer enters the power down mode, it disconnects itself from the USB and
 * can be re-enabled manually only with its local power button. This is a really bad
 * idea for remote printers.
 *
 * @note This command is reverse-engineered, it isn't listed in the programming manual
 * @note The big endian format for the subcommand's data (here @b time_val) is guessed only.
 *       All other printer commands use little endian for multibyte parameter. So, the
 *       command's data might have a different format/content than listed here
 */
int ql_auto_powerdown(struct qldriver *ql, unsigned short time_out)
{
	struct ql_power_command {
		uint8_t escape;
		uint8_t command;
		uint8_t option;
		uint8_t subcommand;
		uint16_t time_val; /* big endian (guessed) */
	} __packed;
	struct ql_power_command auto_power = {
		.escape = '\e',
		.command = 'i',
		.option = 'U',
		.subcommand = 'A',
	};

	auto_power.time_val = htobe16(time_out);
	return ql_printer_data_send(ql, &auto_power, sizeof(auto_power), 0);
}

/**
 * Set the printer's power up behaviour when connecting to external power
 * @param[in] ql Full job description
 * @param[in] mode Power On mode
 *
 * @b mode can be:
 * - 0x0000 Keep offline when connecting to external power
 * - 0x0001 Go online when connecting to external power
 *
 * @note This command is reverse-engineered, it isn't listed in the programming manual
 * @note The big endian format for the subcommand's data (here @b mode) is guessed only.
 *       All other commands use little endian for multibyte parameter. So, the command's
 *       data might have a different format/content than listed here
 */
int ql_auto_powerup(struct qldriver *ql, unsigned short mode)
{
	struct ql_power_command {
		uint8_t escape;
		uint8_t command;
		uint8_t option;
		uint8_t subcommand;
		uint16_t mode; /* big endian (guessed) */
	} __packed;
	struct ql_power_command auto_power = {
		.escape = '\e',
		.command = 'i',
		.option = 'U',
		.subcommand = 'p',
	};

	auto_power.mode = htobe16(mode);
	return ql_printer_data_send(ql, &auto_power, sizeof(auto_power), 0);
}

/**
 * Initialize/reset the printer
 * @param[in] ql Full job description
 * @retval 0 On success
 * @retval Negative Error code from the #ql_printer_data_send() call
 *
 * This command can also be used every time to terminate a current print
 *
 * @note To be sent once at the beginning of a print job (according to the manual)
 */
int ql_reset(struct qldriver *ql)
{
	struct ql_init_command {
		uint8_t escape;
		uint8_t command;
	} __packed;
	static const struct ql_init_command init = {
		.escape = '\e',
		.command = '@',
	};

	/* Let follow the init command */
	return ql_printer_data_send(ql, &init, sizeof(init), 0);
}

/**
 * Start a new print job from the printer's point of view
 * @param[in] ql Full job description
 * @retval 0 On success
 * @retval Negative Error code from the #ql_printer_data_send() call
 *
 * To be sent at the beginning of a new print job or if the current processing has to be stopped
 * at the printer's side.
 *
 * From the datasheet: "If data transmission is to be stopped midway, send
 * the “initialize” command after sending the “invalidate” command for the
 * appropriate number of bytes to return to the receiving state, where the print
 * buffer is cleared."
 *
 * @note @b invalidate means the specified amount of zero bytes
 * @note @b initialize means the *ESC,@* command in #ql_reset()
 *
 * @note To be sent once at the beginning of a print job (according to the manual)
 */
static __nonnull((1)) __wur int ql_invalidate_and_initialize(struct qldriver *ql)
{
	static const unsigned char buffer[16] = { 0x00, };
	size_t count = ql->leading_bytes;
	int rc;

	/* Start with zero bytes */
	while (count >= sizeof(buffer)) {
		rc = ql_printer_data_send(ql, buffer, sizeof(buffer), 0);
		if (rc < 0)
			return rc;
		count -= sizeof(buffer);
	}

	if (count > 0) {
		rc = ql_printer_data_send(ql, buffer, count, 0);
		if (rc < 0)
			return rc;
	}

	/* Let follow the init command */
	return ql_reset(ql);
}

/**
 * Bit to setup the printer into bi-colour print mode. Used in the
 * #ql_extended_mode_set() function as its @b value parameter
 *
 * @note Can be or'ed with the #QL_CUT_AT_END and #QL_HIGH_RESOLUTION bits.
 */
#define QL_BI_COLOR 0x01
/**
 * Bit to setup the printer into "cut when job is finished" mode. Used in the
 * #ql_extended_mode_set() function as its @b value parameter
 *
 * @note It is still unclear what exactly this bit means in conjunction to the
 *       other cutting related bits. If someone has an idea, drop me a note.
 * @note Can be or'ed with the #QL_BI_COLOR and #QL_HIGH_RESOLUTION bits.
 */
#define QL_CUT_AT_END 0x08
/**
 * Bit to setup the printer into "high resolution" mode. E.g. 600 DPI in vertical
 * direction. Used in the #ql_extended_mode_set() function as its @b value parameter
 *
 * @note Can be or'ed with the #QL_BI_COLOR and #QL_CUT_AT_END bits.
 */
#define QL_HIGH_RESOLUTION 0x40

/**
 * Send expanded mode
 * @param[in] ql Full job description
 * @param[in] value Ored list of of #QL_BI_COLOR, #QL_CUT_AT_END or #QL_HIGH_RESOLUTION
 * @retval 0 On success
 * @retval Negative Error code from the #ql_printer_data_send() call
 *
 * - #QL_BI_COLOR Set printer into bi-colour mode (QL8xx with special DK roll only)
 * - #QL_CUT_AT_END Cut only at the end of the job (refer #ql_last_page_print() for details)
 * - #QL_HIGH_RESOLUTION Switch to 600 DPI in vertical direction (never use in combination with #QL_BI_COLOR)
 *   (known for QL8xx printers for now)
 *
 * @note With the cutter related bit 3 cutting at the end (whatever 'end' here
 *       is meant) can be controlled
 *
 * @note To be sent at the beginning of a page and required at each page (according to the manual)
 */
static __nonnull((1)) __wur int ql_extended_mode_set(struct qldriver *ql, unsigned value)
{
	struct ql_simple_command ext_mode = {
		.escape = '\e',
		.command = 'i',
		.option = 'K',
	};

	assert(value < 256);

	/* Let us be really pedantic… */
	if (value & QL_BI_COLOR) {
		if (ql->bi_colour_prn_caps == 0) {
			caps_print_error(_("Internal failure: unsupported bi-colour/device setup detected\n")); /* doc */
			exit(1); /* eat this… */
		}

		if (ql->bi_colour_medium_caps == 0) {
			caps_print_error(_("Internal failure: unsupported bi-colour/medium setup detected\n")); /* doc */
			exit(1); /* eat this… */
		}

		if (value & QL_HIGH_RESOLUTION) {
			caps_print_error(_("Internal failure: unsupported bi-colour/resolution setup detected\n")); /* doc */
			exit(1); /* eat this… */
		}
	}

	ext_mode.value = (uint8_t)value;

	return ql_printer_data_send(ql, &ext_mode, sizeof(ext_mode), 0);
}

/**
 * Bit to setup the printer into "auto cut" mode. Used in the #ql_cut_mode_set()
 * function as its @b mode parameter
 */
#define QL_AUTO_CUT 0x40

/**
 * Setup "various mode"
 * @param[in] ql Full job description
 * @param[in] mode Cutter mode, '0' or #QL_AUTO_CUT
 * @retval 0 On success
 * @retval Negative Error code from the #ql_printer_data_send() call
 *
 * @note Selection between auto and manual?
 * @note Same command for the QL500 but different content (TODO)
 *
 * @note To be sent at the beginning of a page and required at each page (according to the manual)
 */
static __nonnull((1)) __wur int ql_cut_mode_set(struct qldriver *ql, unsigned mode)
{
	struct ql_simple_command cut_mode = {
		.escape = '\e',
		.command = 'i',
		.option = 'M',
	};

	assert(mode < 256);
	cut_mode.value = (uint8_t)mode;

	return ql_printer_data_send(ql, &cut_mode, sizeof(cut_mode), 0);
}

/**
 * Default value for the #ql_cut_on_label_n() function to define "cut each single label"
 * To be used as its @b cut_on_page parameter.
 */
#define QL_CUT_EACH_LABEL 0

/**
 * Setup the label count when to cut
 * @param[in] ql Full job description
 * @param[in] cut_on_page Auto cut after this amount of pages (can be #QL_CUT_EACH_LABEL for cut every label)
 * @retval 0 On success
 * @retval Negative Error code from the #ql_printer_data_send() call
 *
 * @attention Makes sense only if auto-cut is enabled
 *
 * @note Nothing happens (at least with the cutter), if this command wasn't sent.
 *
 * @note To be sent at the beginning of a page and required at each page (according to the manual)
 */
static __nonnull((1)) __wur int ql_cut_on_label_n(struct qldriver *ql, unsigned cut_on_page)
{
	struct ql_simple_command cut_mode = {
		.escape = '\e',
		.command = 'i',
		.option = 'A',
	};

	assert(cut_on_page < 256);

	if (cut_on_page == QL_CUT_EACH_LABEL)
		cut_on_page = 1; /* e.g. cut on each label */

	cut_mode.value = (uint8_t)cut_on_page;

	return ql_printer_data_send(ql, &cut_mode, sizeof(cut_mode), 0);
}

/**
 * Disable the printer's notification capability
 * @param[in] ql Full job description
 * @retval 0 On success
 * @retval Negative Error code from the #ql_printer_data_send() call
 *
 * We do not have a real back channel, so we can't recieve its data
 *
 * @todo But it would be useful to have a back channel!
 */
static __nonnull((1)) __wur int ql_notification_disable(struct qldriver *ql)
{
	struct ql_simple_command nofification = {
		.escape = '\e',
		.command = 'i',
		.option = '!',
	};

	nofification.value = 0x01; /* Means: do not notify */

	return ql_printer_data_send(ql, &nofification, sizeof(nofification), 0);
}

/**
 * Configure the printer to a sepcifc "dynamic mode"
 * @param[in] ql Full job description
 * @retval 0 On success
 * @retval Negative Error code from the #ql_printer_data_send() call
 *
 * For some QL models this has to be done at most once per run-time. The
 * printer operates in this mode until it is turned off again. But read the note below!
 *
 * @note No rule without exception: according to its datasheet, the QL600 requires this command
 *       right after the reset command and requires it again after the #ql_last_page_print()
 *       but with mode 'default' (eg. mode = 0xff)
 *
 * @note To be sent at the beginning of a page and required at each page
 *       (according to the manual). The real requirement is unclear, since the
 *       datasheet lists different requirements at different pages.
 */
static __nonnull((1)) __wur int ql_dynamic_mode(struct qldriver *ql)
{
	struct ql_simple_command mode = {
		.escape = '\e',
		.command = 'i',
		.option = 'a',
	};

	mode.value = 0x01; /* Raster Mode */

	return ql_printer_data_send(ql,&mode, sizeof(mode), 0);
}

#if 0
/**
 * Convert millimeter to dots
 * @param[in] resolution Resolution [DPI]
 * @param[in] length Length in [mm]
 * @return Dots in the given length
 *
 * Most of the time @b resolution will be 300 DPI, but in high resolution some
 * printers are capable to print at 600 DPI (in vertical direction, due to gear).
 * It seems they are all stuck at 300 DPI for the horizontal direction (printer
 * head restriction).
 */
static unsigned convert_mm_to_points(unsigned resolution, double length)
{
	return (unsigned)((double)resolution * fabs(length) / 25.4);
}
#endif

/**
 * Setup the margins (top and bottom) to be used for this print
 * @param[in] ql Full job description
 * @param[in] margins Top and bottom margins [dots] TODO maybe better in mm?
 * @retval 0 On success
 * @retval Negative Error code from the #ql_printer_data_send() call
 *
 * @pre According to the QL800 manual, the margins must be at least 3 mm (or 35 dots)
 *      and max. 127 mm (or 1500 dots).
 *
 * @attention Since the margin is defined in dots, it is resolution dependend (vertical!)
 *
 * @note From the manual: "since a margin amount cannot be specified with die-cut labels,
 *                  this command is sent with a margin amount of 0."@n
 *                  (from section 2.2.3 Explanation of print data for the test page)
 *
 * @verbatim
  Continuous tape:

             cut                                          cut
   -----------|--------------------------------------------|--------------
          |   |   |                                    |   |   |
    ----->|   |   |<------- printable area ----------->|   |   |<---------
          |   |   |                                    |   |   |
   -----------|--------------------------------------------|--------------
          |<->|<->| < margins                margins > |<->|<->|

  Die-cut labels (assumption, but was wrong!):

             cut                                           cut
   -----------|--------------------------------------------|-------------------
   -------+   |   +------------------------------------+   |   +---------------
          |   |   |                                    |   |   |
   ------>|   |   |<---------- printable area -------->|   |   |<--------------
          |   |   |                                    |   |   |
   -------+   |   +------------------------------------+   |   +---+-----------
   -----------|--------------------------------------------|-------------------

  Die-cut labels, margin handling (reality):

             cut                                           cut
   -----------|--------------------------------------------|-------------------
   -------+   |   +------------------------------------+   |   +---------------
          |   |   |  |                              |  |   |   |
   ------>|   |   |  |<------- printable area ----->|  |   |   |<--------------
          |   |   |  |                              |  |   |   |
   -------+   |   +------------------------------------+   |   +---+-----------
   -----------|--------------------------------------------|-------------------
              |<->|<>| < margins          margins > |<>|<->|
                                                         ^---- implicit margin added by the printer
                                                      ^------- implicit margin added by the printer
 * @endverbatim
 *
 * @attention The top and bottom margins of 35 dots are implicit for die-cut labels.
 *            The printer doesn't print in these areas, even if data was sent for them.@n
 *            It is even more worse: data sent for these areas (e.g. lines of data) will
 *            @b not be ignored. The printer increases the size of the label instead.@n
 *            And the next cut is made inside the following label - instead of
 *            inbetween them.
 *
 * @note To be sent at the beginning of a page and required at each page (according to the manual)
 */
static __nonnull((1)) __wur int ql_define_tape_margins(struct qldriver *ql, uint16_t margins)
{
	struct tape_margins {
		uint8_t escape;
		uint8_t command;
		uint8_t option;
		uint16_t margins; /* in little endian */
	} __packed;
	struct tape_margins margin = {
		.escape = '\e',
		.command = 'i',
		.option = 'd',
	};

	if (ql->pre_sized) {
		assert(margins == 0);
	} else {
		/* note: valid at 300 DPI vertical */
		assert(margins >= 35);
		assert(margins <= 1500); /* 127 mm, according to the datasheets */
	}
#if 0
	if (margins > 0) {
		if (ql->high_res) {
			assert(margins >= 70);
			assert(margins < 3000);
		} else {
			assert(margins >= 35);
			assert(margins < 1500);
		}
	}
#endif
	margin.margins = htole16(margins);

	return ql_printer_data_send(ql, &margin, sizeof(margin), 0);
}

/** Unknown meaning in the #print_inform::valid member */
#define QL_WHAT_IS_BIT_0        0x01
#define QL_MEDIA_ENTRY_VALID	0x02
#define QL_WIDTH_ENTRY_VALID	0x04
#define QL_LENGTH_ENTRY_VALID	0x08
// where does this value came from? The datasheet does not mention it!
#define QL_RASTER_ENTRY_VALID	0x10
#define QL_HIGH_PRINT_QUALITY	0x40
/** I found a hint. When set, it means: only print on labels of matching size */
#define QL_RECOVER_ENTRY_VALID	0x80

#define QL_MEDIA_TYPE_CONTINUOUS 0xa
#define QL_MEDIA_TYPE_DIE_CUT 0xb
/** never use while bi-colour printing (according to the datasheet) */
#define QL_QUALITY_PRINT 0x40
/** Always use with bi-colour printing (according to the datasheet) */
#define QL_FAST_PRINT 0x00

/**
 * Send information about the job or the next page
 * @param[in] ql Full job description
 * @param[in] media_type #QL_MEDIA_TYPE_CONTINUOUS or #QL_MEDIA_TYPE_DIE_CUT, negative value for not defined
 * @param[in] media_width Width of the to be printed medium in [mm], negative value for not defined
 * @param[in] media_length Length of the to be printed media in [mm] ('0' for continuous roll), negative value for not defined
 * @param[in] raster Count of rasterlines of one "page", negative value for not defined
 * @param[in] start_page '0' for the first page, '1' for continuous page
 * @param[in] quality #QL_QUALITY_PRINT or #QL_FAST_PRINT
 * @retval 0 On success
 * @retval Negative Error code from the #ql_printer_data_send() call
 *
 * The manual encodes this info (as an example):
 *
 * @par  This is the command for “1.1" × 3.5" (29 mm × 90 mm)” die-cut labels.
 *
 * into:
 * @verbatim
  1b 69 7A 8F 0B 1D 5A DF 03 00 00 00 00
                                   ^^___ Starting page
                       ^^_^^_^^_^^______ 991 raster lines ~83 mm
                    ^^__________________ 90 mm length
                 ^^_____________________ 29 mm width
              ^^________________________ Media type: die cut
           ^^___________________________ 8F: 10x0 1111
                                                     ^_ ????
                                                    ^__ Media type valid
                                                   ^___ Width valid
                                                  ^____ Length valid
                                                ^______ Raster invalid (vague interpretation!)
                                              ^________ fast print
                                             ^_________ Recovery

 @endverbatim
 * For the DK1201 roll (die-cut 29 mm x 90 mm label), the QL800 datasheet defines:
 *  - width: 342 dots
 *  - heigt: 1061 dots (= top margin + printable height + top/bottom margin)
 *  - printable width: 306 dots
 *  - printable height: 991 dots
 *  - left/right margin: 18 dots
 *  - top/bottom margin: 35 dots
 * Note: all at 300 DPI
 *
 * @note To be sent at the beginning of a page and required at each page (according to the manual)
 */
static __nonnull((1)) __wur int ql_print_information(struct qldriver *ql, int media_type, int media_width, int media_length, int raster, int start_page, int quality)
{
	struct print_inform {
		uint8_t escape;
		uint8_t command;
		uint8_t option;
		uint8_t valid; // which of the following fields contain valid data
		uint8_t media_type;
		uint8_t media_width; // in [mm]
		uint8_t media_length; // in [mm], 0 for continuous roll
		uint32_t raster; // size in raster lines of one "page", encoded in little endian
		uint8_t start_page; // 1 start page, 0 continuous page
		uint8_t reserved;
	} __packed;
	struct print_inform info = {
		.escape = '\e',
		.command = 'i',
		.option = 'z',
		.reserved = 0,
	};

	info.valid = QL_RECOVER_ENTRY_VALID | QL_WHAT_IS_BIT_0; /* FIXME bit 0 seems always required? */

	if (media_type >= 0) {
		info.valid |= QL_MEDIA_ENTRY_VALID;
		assert(media_type < 256);
		info.media_type = (uint8_t)media_type;
	}
	if (media_width >= 0) {
		info.valid |= QL_WIDTH_ENTRY_VALID;
		assert(media_width < 256);
		info.media_width = (uint8_t)media_width;
	}
	if (media_length >= 0) {
		info.valid |= QL_LENGTH_ENTRY_VALID;
		assert(media_length < 256);
		info.media_length = (uint8_t)media_length;
	}
	if (raster >= 0) {
//		info.valid |= QL_RASTER_ENTRY_VALID; // TODO not sure about this bit
		info.raster = htole32((uint32_t)raster);
	} else {
		caps_print_warn(_("Raster lenght is undefined. Expect the print fails\n"));
		info.raster = 0; /* just for tests */
	}

	assert(start_page == 0 || start_page == 1);
	assert(start_page <= UINT8_MAX);
	info.start_page = (uint8_t)start_page;

	assert(quality == QL_QUALITY_PRINT || quality == QL_FAST_PRINT);
	info.valid |= (uint8_t)quality;

	return ql_printer_data_send(ql, &info, sizeof(info), 0);
}

/**
 * Send the heading commands to setup the printer prior a print job
 * @param[in] ql Full job description
 *
 * Called once per print job at the beginning
 */
int ql_job_header_generate(struct qldriver *ql)
{
	int rc;

	rc = ql_invalidate_and_initialize(ql);
	if (rc < 0)
		return rc;
	rc = ql_dynamic_mode(ql);
	if (rc < 0)
		return rc;
	return ql_notification_disable(ql);
}

/**
 * Send the footer commands after a print job
 * @param[in] ql Full job description
 *
 * @attention Do not call in case of a procesing failure. These commands make
 *            sense only if the print job was successful.
 *
 * Called once per print job at the end. But only:
 * - if the whole job was successful
 * - no termination request was received
 */
int ql_job_footer_generate(struct qldriver *ql)
{
	return ql_last_page_print(ql);
}

/**
 * @param[in] ql Full job description
 * @param[in] first_time Flag if called the first time in a print job
 * @retval 0 On success
 * @retval -ENODEV Device seems gone (e.g. channel to the next stage is gone)
 *
 * Called once per page at the beginning
 *
 * @note In case of -ENODEV it makes no sense trying to send any further data or commands
 *
 * @pre The #qldriver::trim member must be up to data, e.g. its content matches the @b current raster page header
 *
 * @todo Deal with QL_FAST_PRINT/QL_QUALITY_PRINT for monochrome use case
 * @todo Do the QL_QUALITY_PRINT and QL_HIGH_RESOLUTION bits depend on each other?
 * @todo Deal with auto cut setting and when to cut (and if the printer has a cutter).
 *       To make it simple: only "cut at end" and "cut each" should be supported
 */
int ql_page_header_generate(struct qldriver *ql, int first_time)
{
	unsigned ext_mode;
	int quality;
	int rc;

	if (!first_time) {
		/* First finish the previous page */
		rc = ql_page_print(ql);
		if (rc < 0)
			return rc;
	}
	/*
	 * According to the datasheet, never use #QL_QUALITY_PRINT in
	 * conjunction with bi-color print
	 */
	if (ql->high_density_print)
		quality = QL_QUALITY_PRINT; /* TODO is this assumption correct? See comment in the header */
	else
		quality = QL_FAST_PRINT;

	/* Medium size, raster size, print quality */
	if (ql->pre_sized) {
		assert(ql->trim.media_width_mm <= INT_MAX);
		assert(ql->trim.media_height_mm <= INT_MAX);
		assert(ql->trim.top_keep <= INT_MAX);
		rc = ql_print_information(ql, QL_MEDIA_TYPE_DIE_CUT,
							 (int)ql->trim.media_width_mm, (int)ql->trim.media_height_mm,
							 (int)ql->trim.top_keep,
							 !first_time, quality);
	} else {
		assert(ql->trim.media_width_mm <= INT_MAX);
		assert(ql->trim.top_keep <= INT_MAX);
		rc = ql_print_information(ql, QL_MEDIA_TYPE_CONTINUOUS,
							 (int)ql->trim.media_width_mm, 0,
							 (int)ql->trim.top_keep,
							 !first_time, quality);
	}

	if (rc < 0)
		return rc;

	/*
	 * Bi-colour mode and 600 DPI
	 */
	ext_mode = 0;
	if (ql->coloured_raster)
		ext_mode |= QL_BI_COLOR;
	if (ql->high_density_print)
		ext_mode |= QL_HIGH_RESOLUTION;

	/* Cutter handling */
	if (ql->auto_cutter_caps) {
		rc = ql_cut_mode_set(ql, QL_AUTO_CUT);
		if (rc == 0) {
			rc = ql_cut_on_label_n(ql, QL_CUT_EACH_LABEL);
			if (rc == 0)
				rc = ql_extended_mode_set(ql, ext_mode | QL_CUT_AT_END);
		}
	} else {
		/* no auto cutter available */
		rc = ql_cut_mode_set(ql, 0);
		if (rc == 0)
			rc = ql_extended_mode_set(ql, ext_mode);
	}

	if (rc < 0)
		return rc;

	/* Margin handling */
	if (ql->pre_sized)
		/* According to the datasheets: pre-sized labels need zero margins */
		rc = ql_define_tape_margins(ql, 0);
	else {
		/* According to the datasheets: continuous labels *need* margins */
		if (ql->high_density_print) {
			/* e.g. 3 mm at top and bottom @600 DPI */
			rc = ql_define_tape_margins(ql, PRE_CUT_LABEL_MARGIN_SKIP_HIGH_RES);
		} else {
			 /* e.g. 3 mm at top and bottom @300 DPI */
			rc = ql_define_tape_margins(ql, PRE_CUT_LABEL_MARGIN_SKIP_LOW_RES);
		}
	}

	if (rc < 0)
		return rc;

	/* Always setup compression mode prior sending the data */
	return ql_compression_setup(ql, QL_NO_COMPRESSION);
}

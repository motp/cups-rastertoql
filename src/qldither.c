/*
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * (c) 2021 Juergen Borleis <projects@caps-printing.org>
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 */
#include <assert.h>
#include <limits.h>
#include <sys/types.h>
#include <unistd.h>

#include "qldriver.h"

/**
 * @file qldither.c
 * @brief A collection of half tone algorithems
 *
 * This is a collection of functions to run half-tone algorithms on the
 * raster data prior it is printed.
 *
 * Each algorithm needs two lines of input data with the raster data. They are
 * always processed at once and - important - gets both changed by the algorithms.
 * Raw raster data is expected in shades of grey and in the format of a signed short
 * per pixel. #move_in_next_line() does this required conversation (unsigned char to signed short).
 *
 * After the algorithm has processed the lines, the first line can be printed.
 * Based on the signed short pixel value result, the printer driver needs to chose a
 * corresponding print pattern ("to dot, or not to dot?").
 *
 * After that, the second line from the previous call needs to be the first line
 * in the next call, while the second line then contains fresh data from the
 * next raw raster line. You can think of the algorithm slides from the top to the bottom
 * of the raster. #move_in_next_line() does this required line swap.
 *
 * So: to start at the top of the image, you need both lines filled with data, run the half tone
 * algorithm, then print the top line from the sliding half tone structure. Then you are in the flow
 * until you hit the bottom line of your image. In order to process and print it, you need to fill
 * an empty line (via #move_in_empty_line()), run the half tone algorithm and again print the top
 * line from the sliding half tone structure. Then you're done.
 */

/**
 * Prepare the sliding line buffers
 * @param[in,out] cnv The converter structure to initialiaize
 *
 * In order to run some kind of half-tone processing we need signed values per pixel which must be
 * larger than the incoming pixels data. Incoming pixels are of type 'unsigned char' with their grey values.
 * Due to this conversion we do not need to honor any kind of saturation while
 * calculating the dithered output.
 *
 * @note Does not return in case of memory failure
 * @pre #halftone_converter::pixel_count must be already set and @b not '0'
 */
void sliding_halftone_get(struct halftone_converter *cnv)
{
	static void *nptr = NULL;
	assert(cnv->pixel_count != 0);

	cnv->sliding_lines[0] = reallocarray(nptr, cnv->pixel_count, sizeof(signed short));
	cnv->sliding_lines[1] = reallocarray(nptr, cnv->pixel_count, sizeof(signed short));

	if (cnv->sliding_lines[0] == NULL || cnv->sliding_lines[1] == NULL) {
		caps_print_panic(_("Out of memory while allocating the sliding line buffers")); /* doc */
		exit(EXIT_FAILURE); /* outch! Don't try to be smart */
	}
	cnv->right_left = 0;
}

/**
 * Clean up image processing via sliding line buffers
 * @param[in,out] cnv The converter structure to destroy
 */
void sliding_halftone_put(struct halftone_converter *cnv)
{
	free(cnv->sliding_lines[0]);
	cnv->sliding_lines[0] = NULL;
	free(cnv->sliding_lines[1]);
	cnv->sliding_lines[1] = NULL;
	cnv->pixel_count = 0;
}

/**
 * Move in the next line with raw raster data into the process
 * @param[in,out] cnv The converter structure to destroy
 * @param[in] raw Buffer with raw pixel data (grey scale) in bytes
 *
 * Throw away the content of the current top line, move all lines below one
 * line up and fill the bottom line with new content from the given @a raw line.
 *
 * Currently there are only two lines in the calculation buffer, so "moving" means
 * "swapping" instead.
 *
 * The byte based pixel data is copied into the sliding half tone structure and gets converted
 * to signed short to be able run the half tone algorithm afterwards.
 *
 * @note In order to be able to calculate the bottom line of the image, you need
 *       to input an empty line instead. Empty means: it must contain a value,
 *       which isn't printed. Refer #move_in_empty_line() for details
 *
 * @pre @b raw must point to a buffer with #halftone_converter::pixel_count bytes.
 */
void move_in_next_line(struct halftone_converter *cnv, const unsigned char raw[cnv->pixel_count])
{
	signed short *temp;
	size_t u;

	/* 'Throw away' the current top line by swapping both buffers */
	temp = cnv->sliding_lines[0];
	cnv->sliding_lines[0] = cnv->sliding_lines[1];
	cnv->sliding_lines[1] = temp;

	/*
	 * In order to make the half tone algorithm happy, we need to expand each
	 * unsigned byte into a signed short and fill in this way the new second/bottom line.
	 */
	for (u = 0; u < cnv->pixel_count; u++)
		temp[u] = (signed short)raw[u];
}

/**
 * Move in an empty line into the process
 * @param[in,out] cnv The converter structure to use
 * @param[in] val 'Empty' value (0…255)
 *
 * 'Empty' means @b val must have a value which should not print anything.
 *  For example #COLOUR_VAL_BRIGHT.
 */
void move_in_empty_line(struct halftone_converter *cnv, signed short val)
{
	signed short *temp;
	size_t u;

	/* 'Throw away' the current top line by swapping both buffers */
	temp = cnv->sliding_lines[0];
	cnv->sliding_lines[0] = cnv->sliding_lines[1];
	cnv->sliding_lines[1] = temp;

	for (u = 0; u < cnv->pixel_count; u++)
		temp[u] = val;
}

/**
 * Return the printer's possible value for a given pixel's grey value, e.g. quantize it
 * @param[in] cnv The converter structure to use
 * @param[in] v The grey value from CUPS raster or any dither algorithm
 * @retval Upper #halftone_converter::dotval[1] if @b v is above #halftone_converter::threshold
 * @retval Lower #halftone_converter::dotval[0] if @b v is below #halftone_converter::threshold
 *
 * From the Ghostscript/CUPS raster perspective:
 * - we receive a '255' value for an unprinted dot, which means the colour of the paper
 *   is visible and this usually means 'white'
 * - we receive a '0' value for a printed dot, which means the colour of the ink or toner
 *   should be visible which usually means 'black'
 * - so, with an decreasing value we get a more black pixel
 *
 * @todo Ensure the returned value fits into a 'signed short'
 */
static __nonnull((1)) signed int quantize_grey_pixel(const struct halftone_converter *cnv, signed int v)
{
	return v > cnv->threshold ? cnv->dotval[1] : cnv->dotval[0];
}

/**
 * Calculate a pixel value according to "Floyd-Steinberg error diffusion" formula
 * @param[in] pix_in the plain pixel to quantize
 * @param[in] q_err the quantization error
 * @param[in] factor one of the factors of the algorithm
 * @return Quantized pixel
 *
 * Calculation done here is: result = @b pix_in + ((@b q_err * @b factor) / 16)
 */
static signed short diffuse(signed int pix_in, signed int q_err, signed int factor)
{
	signed int res;

	/* these builtins are available since GCC5 */
	if (__builtin_smul_overflow(q_err, factor, &res)) {
		caps_print_error(_("smul overflow with (%d x %d)\n"), q_err, factor); /* doc */
		return 0; /* FIXME better value */
	}

	res /= 16;

	if (__builtin_sadd_overflow(pix_in, res, &res)) {
		caps_print_error(_("sadd overflow with (%d x %d)\n"), pix_in, res / 16); /* doc */
		return 0; /* FIXME better value */
	}

	assert(res <= SHRT_MAX && res >= SHRT_MIN);
	return (signed short)res;
}

/**
 * Create some random number
 * @return Some pseudo random number, positive and negative
 *
 * @c stdlib.h defines @c RAND_MAX to 2147483647 (same as @c INT_MAX). By substracting
 * (RAND_MAX / 2) the result should always swing around zero with +/- (RAND_MAX / 2).
 *
 * @pre The srandom() call should be done outside this library. Else, the same
 *      pseudo-random values are used over and over again on every run (which
 *      might be a good idea for testing).
 */
static signed int random_number_create(void)
{
	static signed long int sub = RAND_MAX / 2;
	signed long int some_number;

	some_number = random();
	assert(some_number <= INT_MAX); /* be paranoid */

	return (signed int)(some_number - sub);
}

/** Value for the rightward pixel. Just to avoid anonymous numbers */
#define RIGHTWARD 0
/** Value for the downleftward pixel. Just to avoid anonymous numbers */
#define DOWNLEFTWARD 1
/** Value for the downward pixel. Just to avoid anonymous numbers */
#define DOWNWARD 2
/** Value for the downrightward pixel. Just to avoid anonymous numbers */
#define DOWNRIGHTWARD 3

/**
 * Calculate some randomness for the error diffusion to avoid visible pattern
 * @param[in] error The current quantization error
 * @param[out] random Random offset for the four surrounding pixel
 *
 * The larger the quantization error is, the larger the random offsets will be.
 */
static void error_diffusion_randomness_calc(signed int error, signed int random[4])
{
	signed int swing = abs(error);
	signed int random_value;

	if (swing == 0) {
		random[RIGHTWARD] = 0;
		random[DOWNLEFTWARD] = 0;
		random[DOWNWARD] = 0;
		random[DOWNRIGHTWARD] = 0;
		return;
	}

	random_value = random_number_create();
	random[RIGHTWARD] = random_value % swing; /* For the pixel rightward */

	random_value = random_number_create();
	random[DOWNLEFTWARD] = random_value % swing; /* For the pixel down leftward */

	random_value = random_number_create();
	random[DOWNWARD] = random_value % swing; /* For the pixel downward */

	random_value = random_number_create();
	random[DOWNRIGHTWARD] = random_value % swing; /* For the pixel down rightward */
}

/**
 * Apply error diffusion from right to left
 * @param[in,out] cnv The converter structure to use
 *
@verbatim
   <------------------ processing direction
   | 7/16   *    N   | <-- cnv->sliding_lines[0]
   | 1/16  5/16 3/16 | <-- cnv->sliding_lines[1]
@endverbatim
 *
 * @b N previous pixel, @b * current pixel to process (e.g. X/Y position)
 */
static __nonnull((1)) void error_diffusion_right_to_left(struct halftone_converter *cnv)
{
	signed int old_pixel, new_pixel, quant_error;
	signed short *top_line, *bottom_line;
	signed int random[4];
	size_t x;

	top_line = cnv->sliding_lines[0];
	bottom_line = cnv->sliding_lines[1];

	/* Start from the right */
	x = cnv->pixel_count - 1;

	/* Rightmost pixel */
	old_pixel = top_line[x];
	new_pixel = quantize_grey_pixel(cnv, old_pixel);
	top_line[x]  = (signed short)new_pixel; /* Final value for this pixel */
	quant_error = old_pixel - new_pixel;
	if (quant_error != 0) {
		/* Distribute the quantization error to the surrounding pixels */
		error_diffusion_randomness_calc(quant_error, random);
		top_line[x - 1] = diffuse(top_line[x - 1], quant_error + random[RIGHTWARD], 7); /* mirrored: Pixel rightward */
		bottom_line[x] = diffuse(bottom_line[x], quant_error + random[DOWNWARD], 5); /* Pixel downward */
		bottom_line[x - 1] = diffuse(bottom_line[x - 1], quant_error + random[DOWNRIGHTWARD], 1); /* mirrored: Pixel down rightward */
	}

	x--;

	/* all inbetween */
	for ( ; x > 0; x--) {
		old_pixel = top_line[x];
		new_pixel = quantize_grey_pixel(cnv, old_pixel);
		top_line[x] = (signed short)new_pixel; /* Final value for this pixel */
		/* Calculate quantization error made for this pixel */
		quant_error = old_pixel - new_pixel;
		if (quant_error == 0)
			continue; /* Matches perfect, do not add random and continue with the next pixel */

		error_diffusion_randomness_calc(quant_error, random);

		/* Distribute the quantization error to the surrounding pixels */
		top_line[x - 1] = diffuse(top_line[x - 1], quant_error + random[RIGHTWARD], 7); /* Pixel rightward */
		bottom_line[x - 1] = diffuse(bottom_line[x - 1], quant_error + random[DOWNRIGHTWARD], 1); /* Pixel down rightward */
		bottom_line[x + 1] = diffuse(bottom_line[x + 1], quant_error + random[DOWNLEFTWARD], 3); /* Pixel down leftward */
		bottom_line[x] = diffuse(bottom_line[x], quant_error + random[DOWNWARD], 5); /* Pixel downward */
	}

	/* Leftmost pixel */
	old_pixel = top_line[x];
	new_pixel = quantize_grey_pixel(cnv, old_pixel);
	top_line[x]  = (signed short)new_pixel; /* Final value for this pixel */
	quant_error = old_pixel - new_pixel;
	if (quant_error == 0)
		return;

	/* Distribute the quantization error to the surrounding pixels */
	error_diffusion_randomness_calc(quant_error, random);
	bottom_line[x] = diffuse(bottom_line[x], quant_error + random[DOWNWARD], 5); /* Pixel downward */
	bottom_line[x + 1] = diffuse(bottom_line[x + 1], quant_error + random[DOWNRIGHTWARD], 1); /* mirrored: Pixel down rightward */
}

/**
 * Apply error diffusion from left to right
 * @param[in,out] cnv The converter structure to use
 *
@verbatim
   ------------------> processing direction
   |  N     *   7/16 | <-- cnv->sliding_lines[0]
   | 3/16  5/16 1/16 | <-- cnv->sliding_lines[1]
@endverbatim
 *
 * @b N previous pixel, @b * current pixel to process (e.g. X/Y position)
 */
static __nonnull((1)) void error_diffusion_left_to_right(struct halftone_converter *cnv)
{
	signed int old_pixel, new_pixel, quant_error;
	signed short *top_line, *bottom_line;
	signed int random[4];
	size_t x;

	top_line = cnv->sliding_lines[0];
	bottom_line = cnv->sliding_lines[1];

	x = 0; /* start from the left */

	/* Leftmost pixel */
	old_pixel = top_line[x];
	new_pixel = quantize_grey_pixel(cnv, old_pixel);
	top_line[x]  = (signed short)new_pixel; /* Final value for this pixel */
	quant_error = old_pixel - new_pixel;
	if (quant_error != 0) {
		/* Distribute the quantization error to the surrounding pixels */
		error_diffusion_randomness_calc(quant_error, random);
		top_line[x + 1] = diffuse(top_line[1], quant_error + random[RIGHTWARD], 7); /* Pixel rightward */
		bottom_line[x] = diffuse(bottom_line[x], quant_error + random[DOWNWARD], 5); /* Pixel downward */
		bottom_line[x + 1] = diffuse(bottom_line[x + 1], quant_error + random[DOWNRIGHTWARD], 1); /* mirrored: Pixel down rightward */
	}

	x++;

	/* all inbetween */
	for ( ; x < cnv->pixel_count - 2; x++) {
		old_pixel = top_line[x];
		new_pixel = quantize_grey_pixel(cnv, old_pixel);
		top_line[x] = (signed short)new_pixel; /* Final value for this pixel */
		quant_error = old_pixel - new_pixel;
		if (quant_error == 0)
			continue; /* Matches perfect, do not add random and continue with the next pixel */

		error_diffusion_randomness_calc(quant_error, random);

		/* Distribute the quantization error to the surrounding pixels */
		top_line[x + 1] = diffuse(top_line[x + 1], quant_error + random[RIGHTWARD], 7); /* Pixel rightward */
		bottom_line[x + 1] = diffuse(bottom_line[x + 1], quant_error + random[DOWNRIGHTWARD], 1); /* Pixel down rightward */
		bottom_line[x] = diffuse(bottom_line[x], quant_error + random[DOWNWARD], 5); /* Pixel downward */
		bottom_line[x - 1] = diffuse(bottom_line[x - 1], quant_error + random[DOWNLEFTWARD], 3); /* Pixel down leftward */
	}

	/* Rightmost pixel */
	old_pixel = top_line[x];
	new_pixel = quantize_grey_pixel(cnv, old_pixel);
	top_line[x]  = (signed short)new_pixel; /* Final value for this pixel */
	quant_error = old_pixel - new_pixel;
	if (quant_error == 0)
		return;

	/* Distribute the quantization error to the surrounding pixels */
	error_diffusion_randomness_calc(quant_error, random);
	bottom_line[x] = diffuse(bottom_line[x], quant_error + random[DOWNWARD], 5); /* Pixel downward */
	bottom_line[x - 1] = diffuse(bottom_line[x - 1], quant_error + random[DOWNRIGHTWARD], 1); /* mirrored: Pixel down rightward */
}

/**
 * Quantize the top line according to the "Floyd-Steinberg error diffusion" and distribute the error around
 * @param[in,out] cnv The converter structure to use
 * @return The quantized line in #halftone_converter::sliding_lines[0]
 *
 * From: http://www.visgraf.impa.br/Courses/ip00/proj/Dithering1/floyd_steinberg_dithering.html
 *
@verbatim
   |  N     *   7/16 | <-- cnv->sliding_lines[0]
   | 3/16  5/16 1/16 | <-- cnv->sliding_lines[1]
@endverbatim
 *
 * @b N previous pixel, @b * current pixel to process (e.g. X/Y position)
 *
 * @pre #halftone_converter::sliding_lines[0] and #halftone_converter::sliding_lines[1] must be valid
 * @pre At least three pixels must be in the lines
 */
void halftone_line_with_error_diffusion(struct halftone_converter *cnv)
{
	assert(cnv->pixel_count >= 3);

	if (cnv->right_left) {
		error_diffusion_right_to_left(cnv);
		cnv->right_left = 0;
	} else {
		error_diffusion_left_to_right(cnv);
		cnv->right_left = 1;
	}
}

/**
 * Quantize the top line according to an "ordered dithering matrix"
 * @param[in,out] cnv The converter structure to use
 * @return The quantized line in #halftone_converter::sliding_lines[0]
 *
 * This is a simple algorithm with a 2x2 "ordered dithering matrix":
 *
@verbatim
   | 1/10  3/10 | <-- cnv->sliding_lines[0]
   | 4/10  2/10 | <-- cnv->sliding_lines[1]
@endverbatim
 *
 * http://www.visgraf.impa.br/Courses/ip00/proj/Dithering1/ordered_dithering.html
 *
 * @pre #halftone_converter::sliding_lines[0] and #halftone_converter::sliding_lines[1] must be valid
 */
void halftone_line_ordered(struct halftone_converter *cnv)
{
	static const signed short threshold[4] = { 230, 179, 153, 204, };
	signed short *top_line, *bottom_line;
	size_t x;

	assert(cnv->pixel_count != 0);

	top_line = cnv->sliding_lines[0];
	bottom_line = cnv->sliding_lines[1];
#if 0
	/* TODO liefert nur weiß! */
	for (x = 0; x < cnv->pixel_count; x += 2) {
		top_line[x] = quantize_grey_pixel(cnv, 64 + top_line[x]);
		if (x < cnv->pixel_count - 1)
			top_line[x + 1] = quantize_grey_pixel(cnv, 192 + top_line[x + 1]);
		bottom_line[x] = quantize_grey_pixel(cnv, 128 + bottom_line[x]);
		if (x < cnv->pixel_count - 1)
			bottom_line[x + 1] = quantize_grey_pixel(cnv, bottom_line[x + 1]);
	}
#else
	/* liefert merkwürdige Werte */
	for (x = 0; x < cnv->pixel_count; x += 2) {
		/* everything above the thresholds should be printed */
		top_line[x] = (signed short)(top_line[x] > threshold[0] ? cnv->dotval[1] : cnv->dotval[0]);
		if (x < cnv->pixel_count - 1)
			top_line[x + 1] = (signed short)(top_line[x + 1] > threshold[1] ? cnv->dotval[1] : cnv->dotval[0]);
		bottom_line[x] = (signed short)(bottom_line[x] > threshold[2] ? cnv->dotval[1] : cnv->dotval[0]);
		if (x < cnv->pixel_count - 1)
			bottom_line[x + 1] = (signed short)(bottom_line[x + 1] > threshold[3] ? cnv->dotval[1] : cnv->dotval[0]);
	}
#endif
}

/**
 * Quantize the top line without any half tone algorithm
 * @param[in,out] cnv The converter structure to use
 * @return The quantized line in #halftone_converter::sliding_lines[0]
 *
 * http://www.visgraf.impa.br/Courses/ip00/proj/Dithering1/average_dithering.html
 */
void halftone_line_no_dither(struct halftone_converter *cnv)
{
	signed short *top_line;
	size_t x;

	assert(cnv->pixel_count != 0);

	top_line = cnv->sliding_lines[0];
	for (x = 0; x < cnv->pixel_count; x++)
		top_line[x] = (signed short)quantize_grey_pixel(cnv, top_line[x]);
}

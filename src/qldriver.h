#pragma once
/*
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * (c) 2021 Juergen Borleis <projects@caps-printing.org>
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 */

#include <features.h>
#include <cups/raster.h>
#include <cups/ppd.h>

__BEGIN_DECLS

#include "internal.h"

/**
 * Value of black. The minimal value of a byte (regarding the CUPS raster)
 */
#define COLOUR_VAL_DARK 0x00
/**
 * Value of white. The maximal value of a byte (regarding the CUPS raster)
 */
#define COLOUR_VAL_BRIGHT 0xff

/**
 * The value of a to be printed dot (regarding the internal processing)
 */
#define PRINT_DOT_VAL 0x00

/**
 * The generic count of bytes of one line of these QL printers
 *
 * @note This is valid only for a couple types of these printers. It should be
 *       set in the PPD instead.
 */
#define DEFAULT_QL_LINE_SIZE 90

/**
 * Keep the numbers here in sync with the PPD "cupsModelNumber" entry
 */
enum printer_type {
	BROTHER_QL_UNKNOWN = 0,
	BROTHER_QL_500 = 500,
	BROTHER_QL_550 = 550,
	BROTHER_QL_560 = 560,
	BROTHER_QL_570 = 570,
	BROTHER_QL_580 = 580,
	BROTHER_QL_600 = 600,
	BROTHER_QL_650 = 650,
	BROTHER_QL_700 = 700,
	BROTHER_QL_710 = 710,
	BROTHER_QL_720 = 720,
	BROTHER_QL_800 = 800,
	BROTHER_QL_810 = 810,
	BROTHER_QL_820 = 820,
	BROTHER_QL_1050 = 1050,
	BROTHER_QL_1060 = 1060,
};

/**
 * Selected half-tone algorithm
 */
enum half_tone {
	HT_ERROR_DIFFUSION = 1, /**< Floyd-Steinberg error diffusion */
	HT_ORDERED = 2, /**< Ordered dithering matrix */
	HT_NONE = 3, /**< Just quantize the raster */
};

struct monochrome_line_start;
struct colour_line_start;

struct line_command {
	size_t bytes_per_line; /**< Count of expected command data bytes to describe one line */
	size_t bytes_per_command; /**< Size of @b command in bytes (to be sent to the printer) */
	union {
		struct monochrome_line_start *bw;
		struct colour_line_start *cl;
	} command;
};

/** RGB */
struct qlrgb {
	unsigned char red;
	unsigned char green;
	unsigned char blue;
} __packed;

/** HSV */
struct qlhsv {
	double hue;			/**< 0…360 [degree] */
	double saturation;	/**< 0…100 [percent] */
	double value;		/**< 0…100 [percent] */
};

/**
 * @note The 'signed short' dot values are for calculation only. Feed-in contains values 0x00…0xff
 *       and also after the processing they contain only @b dotval[0] or @b dotval[1] values.
 */
struct halftone_converter {
	unsigned right_left:1; /**< half tone algorithm direction left->right or right->left */
	signed int dotval[2]; /**< dotval[0]: do not print value (e.g. 0), dotval[1]: print value (e.g. 255) FIXME nicht anders herum? */
	signed int threshold; /**< Threshold to chose one of the @b dotval values, below threshold: dotval[0], else dotval[1] */
	size_t pixel_count; /**< width of target image in pixels */
	signed short *sliding_lines[2]; /**< Two lines of the source image to process, size corresponds to "pixel_count" */
};

/**
 * Amount of raster lines to skip at the top and bottom of a pre-cut label.
 * This is a printer device requirement. It more or less defines a margin of 3 mm.@n
 * 35 lines are valid for 300 DPI
 */
#define PRE_CUT_LABEL_MARGIN_SKIP_LOW_RES 35

/**
 * Amount of raster lines to skip at the top and bottom of a pre-cut label.
 * This is a printer device requirement. It more or less defines a margin of 3 mm.@n
 * 70 lines are valid for 600 DPI
 */
#define PRE_CUT_LABEL_MARGIN_SKIP_HIGH_RES 70

/**
 * Trim the input raster to the available media
 *
 * @note For a continuous length tape for now
 *
 * Horizontal:
 *
 * Wider raster image:
@verbatim
        |<--------------------------- printable area width ----------------------------->|
        |<------------------------------ media width ----------------------------------->|
  |<---------------------------------  wider input raster  ---------------------------------->|
  |<--->| "left_skip"
        |<------------------------------------------------------------------------------>| "left_keep"
@endverbatim
 * Smaller raster image:
@verbatim
        |<--------------------------- printable area width ----------------------------->|
        |<------------------------------ media width ----------------------------------->|
                |<-------------- smaller input raster ---------------->|
        |<----->| "left_offset"
                |<---------------------------------------------------->| "left_keep"

@endverbatim
 *
 * @b left_offset is based on required hardware margins.
 *
 * Vertical:
 *
 * On a continous length tape there is no vertical limit to consider.
 *
 * @attention All values are resolution dependend.
 */
struct ql_trim {
	unsigned raster_width; /**< Input raster dot count per line */
	unsigned raster_height; /**< Input raster line count */

	unsigned media_width; /**< Output media dot count per line, always smaller or equal to the printable area */
	unsigned media_height; /**< Output media line count */

	float media_width_mm; /**< Output media width [mm] */
	float media_height_mm; /**< Output media length [mm] */

	unsigned top_skip; /**< Skip raster lines at top and bottom, for pre-cut labels */
	unsigned top_keep; /**< Use raster lines (below @b top_skip) */

	unsigned left_skip; /**< Skip raster dots from the left border if the raster is wider than the media */
	unsigned left_offset; /**< Empty dots on the media before the left dot */
	unsigned left_keep; /**< Use raster dots (right of @b left_skip)*/
};

struct qldriver {
	/* Printer related settings */
	unsigned bytes_per_line; /**< Hard printer limit */
	enum printer_type printer;
	unsigned bi_colour_prn_caps:1; /**< Set, if the printer is able to print a second colour (red) */
	unsigned bi_colour_medium_caps:1; /**< Set, if the medium provides a second colour */
	unsigned high_density_caps:1; /**< Set, if the printer can deal with 600 DPI (in the vertical direction) */
	unsigned auto_cutter_caps:1; /**< Set, if the printer has an autocutter */
	unsigned leading_bytes; /**< Amount of leading zero bytes prior starting print data */
	unsigned min_margin; /**< minimal margin on continuous length tape at top and bottom [mm] */
	unsigned max_margin; /**< maximal margin on continuous length tape at top and bottom [mm] */
	float left_hardware_margin;

	/* Raster related settings */
	ppd_file_t *ppd;
	int fd; /**< CUPS raster file descriptor, real file or stdin */
	cups_raster_t *ras; /**< CUPS raster handle */
	struct cups_page_header2_s raster_header; /**< CUPS raster header info of the current page */
	enum half_tone half_tone_type;
	unsigned pre_sized:1; /**< Set, if the media is of die-cut type, else continous length */
	unsigned coloured_raster:1; /**< Set, if the current raster should be printed in bi-colour */
	unsigned high_density_print:1; /**< Set, if the current print should be done with 600 DPI */
	struct ql_trim trim;

	/* For the red detection (QL8xx specific) */
	double lower_red_angle;
	double upper_red_angle;
	double lower_red_sat;
	double lower_red_val;
};


void sliding_halftone_get(struct halftone_converter *cnv) __nonnull((1));
void sliding_halftone_put(struct halftone_converter *cnv) __nonnull((1));
void halftone_line_no_dither(struct halftone_converter *cnv) __nonnull((1));
void halftone_line_ordered(struct halftone_converter *cnv) __nonnull((1));
void halftone_line_with_error_diffusion(struct halftone_converter *cnv) __nonnull((1));
void move_in_next_line(struct halftone_converter *cnv, const unsigned char raw[cnv->pixel_count]) __nonnull((1,2));
void move_in_empty_line(struct halftone_converter *cnv, signed short val) __nonnull((1));
int cups_ql_driver_init(struct qldriver *ql) __nonnull((1)) __wur;

/**
 * Capabilities of a DK roll
 */
enum dk_caps {
	DK_CAP_UNKNOWN = 0,
	DK_CAP_CONTINUOUS = 1, /**< Continuous length label */
	DK_CAP_DIE_CUT = 2, /**< Pre-cut label */
	DK_CAP_RED = 4, /**< Bi-colour possible (black/red on white) */
};

struct dk_roll_types {
	const char *dkname; /**< always the Brother name of the label cassette */
	const char *like; /**< if possible, points to a compatible label cassette */
	enum dk_caps caps; /**< DK rolls Capabilities */
	float margin; /**< left margin until the print can start, e.g. hits paper (in [mm]) */
};

int ql_dk_information_get(struct dk_roll_types *medium, const char *name) __nonnull((1,2));

int cups_ql_job_print(struct qldriver *ql) __nonnull((1)) __wur;
int cups_ql_colour_page_print(struct qldriver *ql, int first_time) __nonnull((1)) __wur;
int cups_ql_monochome_page_print(struct qldriver *ql, int first_time) __nonnull((1)) __wur;

int cups_ql_raster_header_read(struct qldriver *ql) __nonnull((1)) __wur;
int cups_ql_next_line_read(struct qldriver *ql, size_t  sz, unsigned char buf[sz] __nonstring) __nonnull((1,3)) __wur;

int ql_printer_data_send(struct qldriver *ql, const void *data, size_t count, int flush) __nonnull((1,2)) __wur;

void transform_to_monochrome(size_t input_cnt, const signed short input[input_cnt], unsigned char *data) __nonnull((2,3));

int ql_reset(struct qldriver *ql) __nonnull((1)) __wur;
int ql_auto_powerdown(struct qldriver *ql, unsigned short time_out) __nonnull((1)) __wur;
int ql_auto_powerup(struct qldriver *ql, unsigned short mode) __nonnull((1)) __wur;

int ql_input_raster_crop(struct qldriver *ql) __nonnull((1)) __wur;
int ql_raster_limits_get(struct qldriver *ql) __nonnull((1)) __wur;

/* Job related functions to control the print */
int ql_page_header_generate(struct qldriver *ql, int first_time) __nonnull((1)) __wur;
int ql_job_footer_generate(struct qldriver *ql) __nonnull((1)) __wur;
int ql_job_header_generate(struct qldriver *ql) __nonnull((1)) __wur;

/**
 * Structure of a simple command of a QL series printer
 */
struct ql_simple_command {
	uint8_t escape; /**< Escape code */
	uint8_t command; /**< The command itself, e.g. some ASCII code */
	uint8_t option; /**< Option of this command */
	uint8_t value; /**< Option's value */
} __packed;

#ifdef TEST

int test_cups_ql_next_colour_line_read(struct qldriver *ql, struct halftone_converter *cnv_black, struct halftone_converter *cnv_red);
signed int test_is_red(struct qldriver *ql, const struct qlrgb *pixel);
void test_rgb2hsv(struct qlhsv *hsv, const struct qlrgb *rgb_pixel);
void test_red_dot_mark(signed int val, signed short *black_pixel, signed short *red_pixel);
void test_black_dot_mark(signed int val, signed short *black_pixel, signed short *red_pixel);
void test_transform_to_monochrome(size_t input_cnt, const signed short input[input_cnt], unsigned char *data);

#endif

__END_DECLS

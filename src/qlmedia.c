/*
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * (c) 2021 Juergen Borleis <projects@caps-printing.org>
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 */
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>

#include "qldriver.h"

/**
 * @file qlmedia.c
 * @brief Common routines to deal with DK rolls for QL label printers
 * @copyright GNU General Public License 2.0 or later
 * @author Jürgen Borleis
 *
 * Media handling in QL label printers is a little bit confusing. The printers
 * have some - lets call it 'interesting' - behaviour when dealing with continuous
 * and pre-cut labels. The differences should be handled here.
 */

/**
 * @page how_to_defining_labels How to define labels
 *
 * @subsection continuous_labels Continuous Labels
 *
 * Continuous labels are simple to create: Just create a label with the exact
 * size of your continuous roll paper. E.g. with its exact width.
 *
 * @attention The length of your label must be shorter than some hard limits the
 *            printer has. All I know yet is, you should not exceed a length of
 *            1000 mm per single label (or: between cuts).
 * @attention There is also a minimal limit for the length of your label: it is
 *            about 13 mm or 25 mm (depending on the QL model. Read the manual
 *            of your printer!).
 *
 * The margin at the left and right borders you should not print to, are about
 * 0.5 mm. There are no margin requirements at the top and bottom borders.
 *
 * @attention The printer will always add 3 mm at the top and bottom to your
 *            label. Then it will cut the roll.
 *
 * @b Example:
 *
 * You are using a @b DK2113 roll. It provides a medium width of 62 mm. You define
 * a label with 62 mm width and 100 mm length.
 *
 * @image html label_continuous.svg "Some label defined on a continuous medium"
 *
 * Your printable area on this label is 61 mm wide and has a length of 100 mm.
 *
 * The size of this label after the printer has spit it out is 62 mm wide and
 * has a length of 106 mm.
 *
 * @subsection pre_cut_labels Pre-cut Labels
 *
 * Pre-cut labels are a bit more complicated. The printers have some annoying
 * behaviours, which we must honor to get an expected result.
 *
 * First we must create a label of the exact size of the pre-cut labels of our
 * DK roll. Since the printer adds some margins by itself, we now need to define
 * exact margins. We need to know, the printer will @b always add 3 mm at the
 * top and the bottom of each label. So it depends on the orientation of our
 * label, where we need to honor these margins.
 *
 * If we use a pre-cut label DK roll oriented in portrait, the 3 mm margins are
 * at the short edges. For DK rolls oriented in landscape, the 3mm margins are
 * at their long edges.
 *
 * @image html label_orientation.svg "Some kind of labels in portrait orientation (left) and landscape orientation (right)"
 *
 * The margins at the other edges are about 0.5 mm (drop me a note, if this isn't
 * always true). But refer @ref hardware_assumption for more details.
 *
 * With this rule it should be easy to define the correct margins, independently
 * of your label's orientation.
 *
 * You may also read the @ref how_the_filter_deals_with_margins.
 *
 * @b Example:
 *
 * You are using a DK1201 roll. It provides pre-cut labels of the size 29 mm
 * by 90 mm. Orientation on the roll is portrait. Thus, you need the 3 mm
 * margins at the 29 mm edge.
 *
 * @image html label_landscape_to_portrait.svg "Landscape label, but printed in portrait orientation"
 *
 * You can create a label in landscape (29 mm height, 90 mm width), with 3 mm
 * margins at its left and right edge and 0.5 mm at its top and bottom edge. But
 * regarding the top and bottom margins also refer @ref hardware_assumption for
 * more details.
 *
 * @subsubsection wrong_pre_cut_labels Wrong Size for a pre-cut Label
 *
 * What happens, if you create a larger label than the pre-cut label is? The
 * printer won't be able to cut inbetween labels! It will cut inside the next
 * label instead.
 *
 * @image html wrong_label_size.svg "Wrong cut due to wrong vertical size"
 *
 * After that is happen, you must manually remove this remaining label, else the
 * printer can no longer feed it correctly.@n
 * Solution: Pull out the DK roll from the printer and manually cut the tape at
 * the @b intended position, and insert it into the printer again.
 */

/**
 * List of known DK rolls and their special requirements. At least we need to
 * know here if the DK rolls are continuous or pre-cut.
 * And since the QL8xx printer variants we also need to know, if the DK roll
 * can be used for bi-colour prints.@n
 * But most of the time, they are more or less the same from the printer point
 * of view, so the @b .like member points to a base DK roll type.
 *
 * @todo All transport tape sizes are still to be checked
 */
static const struct dk_roll_types rolls[] = {
	{
		.dkname = "small_size_small_margin", /* Dummy entry */
		.margin = 1.5, /* [mm] */
	}, {
		.dkname = "full_size_large_margin", /* Dummy entry */
		.margin = 2.0, /* [mm] */
	}, {
		.dkname = "DK1201", /* Standard Address Label, 32 mm tape, 29 mm x 90 mm paper */
		.like = "small_size_small_margin",
		.caps = DK_CAP_DIE_CUT,
	}, {
		.dkname = "DK1202", /* Shipping Label, 66 mm tape, 62 mm x 100 mm paper */
		.like = "full_size_large_margin",
		.caps = DK_CAP_DIE_CUT,
	}, {
		.dkname = "DK1203", /* File folder label, FIXME, 17 mm x 88 mm paper */
		.like = "small_size_small_margin",
		.caps = DK_CAP_DIE_CUT,
	}, {
		.dkname = "DK1204", /* White multi-purpose/return address labels, FIXME, 17 mm x 54 mm paper */
		.like = "DK1201",
	}, {
		.dkname = "DK1207", /* CD/DVD film labels, FIXME, 60 mm round paper */
		.like = "DK1201",
	}, {
		.dkname = "DK1208", /* White large address labels, FIXME, 38 mm x 90 mm paper */
		.like = "DK1201",
	}, {
		.dkname = "DK1209", /* White small address labels, FIXME, 29 mm x 62 mm paper */
		.like = "DK1201",
	}, {
		.dkname = "DK1218", /* White Round Paper Adhesive Labels, FIXME, 24 mm */
		.like = "DK1201",
	}, {
		.dkname = "DK1219", /* Round White Paper Labels, FIXME, 12 mm */
		.like = "DK1218",
	}, {
		.dkname = "DK1221", /* Square White Labels, FIXME, 23 mm x 23 mm */
		.like = "DK1201",
	}, {
		.dkname = "DK1234", /* Self Adhesive Name Badge Labels, FIXME, 60 mm x 86 mm */
		.like = "DK1201",
	}, {
		.dkname = "DK1240", /* Large Multipurpose White Paper Labels, FIXME, 50,5 mm x 101 mm -> QL1050, QL1050N, QL1060N, QL1100, QL1110NWB */
		.like = "DK1201",
	}, {
		.dkname = "DK1241", /* Large Shipping White Paper Labels, FIXME, 101 mm x 152 mm -> QL1050, QL1050N, QL1060N, QL1100, QL1110NWB  */
		.like = "DK1201",
	}, {
		.dkname = "DK1247", /* Large Shipping White Paper Labels, FIXME, 103 mm x 164 mm -> QL-1050, QL-1060N, QL-1100, QL-1110NWB, QL-1050N */
		.like = "DK1201",
	}, {
		.dkname = "DK2113", /* Continuous clear film tape labels, FIXME, 62 mm paper */
		.like = "full_size_large_margin",
		.caps = DK_CAP_CONTINUOUS,
	}, {
		.dkname = "DK2205", /* Continuous white paper tape labels, FIXME, 62 mm paper */
		.like = "DK2113",
	}, {
		.dkname = "DK2210", /* Continuous white paper tape labels, FIXME, 62 mm paper */
		.like = "DK2113",
	}, {
		.dkname = "DK2211", /* Continuous white film tape labels, FIXME, 29 mm paper */
		.like = "small_size_small_margin",
		.caps = DK_CAP_CONTINUOUS,
	}, {
		.dkname = "DK2212", /* Continuous white film tape labels, FIXME, 62 mm paper */
		.like = "DK2113",
	}, {
		.dkname = "DK2214", /* Narrow Width Continuous Length Paper Tape, FIXME, 12 mm */
		.like = "DK2211",
	}, {
		.dkname = "DK2223", /* White Continuous Length Paper Tap, FIXME, 50mm */
		.like = "DK2211",
	}, {
		.dkname = "DK2225", /* White Continuous Length Paper Tape, FIXME, 38 mm */
		.like = "DK2211",
	}, {
		.dkname = "DK2243", /* White Continuous Length Paper Tape, FIXME, 101 mm -> QL1050, QL1050N, QL1060N, QL1100, QL1110NWB */
		.like = "DK2113",
	}, {
		.dkname = "DK2246", /* White Continuous Length Paper Tape, FIXME, 103 mm ->  QL1050, QL1050N, QL1060N, QL1100, QL1110NWB */
		.like = "DK2113",
	}, {
		.dkname = "DK2251", /* Continuous white paper tape labels, 66 mm tape, 62 mm paper, bi-colour */
		.like = "full_size_large_margin",
		.caps = DK_CAP_CONTINUOUS | DK_CAP_RED,
	}, {
		.dkname = "DK2606", /* Yellow Continuous Length Film Tape, 66 mm tape, 62 mm paper */
		.like = "DK2211",
	}, {
		.dkname = "DK3235", /* Small Removable White Paper Labels, FIXME, 29 mm x 54 mm -> listed for QL-800, QL-810W, QL-820NWB. Why? */
		.like = "DK1201",
	}, {
		.dkname = "DK4205", /* White Removable Continuous Length Paper Tape, 66 mm tape, 62 mm paper */
		.like = "DK2113",
	}, {
		.dkname = "DK4605", /* Yellow Removable Continuous Length Paper Tape, 66 mm tape, 62 mm paper */
		.like = "DK2113",
	}, {
		.dkname = "DKN5224", /* White Non-Adhesive Continuous Length Paper Tape, FIXME, 54 mm */
		.like = "DK2211",
	}, {
		.dkname = "DK11201", /* Standard adress label 32 mm tape, 29 mm x 90 mm paper */
		.like = "DK1201",
	}, {
		.dkname = "DK11209", /* Small adress label, 66 mm tape, 62 mm x 29 mm paper */
		.like = "DK1202",
	}, {
		.dkname = "DK22211", /* Continuous white film, 32 mm tape, 29 mm paper */
		.like = "DK2211",
	}, {
		.dkname = "DK22212", /* Continuous white film, 66 mm tape, 62 mm paper */
		.like = "DK2212",
	}, {
		.dkname = "DK44205", /* Like DK2212, but "removeable" */
		.like = "DK2212",
	},
};

/**
 * Loop through the list of rolls and search for a specific one
 * @param[in] name The DK roll name to search for
 * @retval Pointer The corresponding roll element
 * @retval NULL @b name not found
 */
static __nonnull((1)) const struct dk_roll_types *ql_dk_element_get(const char *name)
{
	size_t u;

	for (u = 0; u < ARRAY_SIZE(rolls); u++) {
		if (!strcasecmp(name, rolls[u].dkname))
			return &rolls[u];
	}

	return NULL;
}

/**
 * Retrieve settings/information about a specific DK roll/cassette
 * @param[out] medium The settings for the DK roll named by @b name
 * @param[in] name The DK roll name the settings should be retrieved for
 * @retval 0 On success, @b *medium is valid
 * @retval -EINVAL Roll with the given @b name not found
 *
 * DK rolls can inherit settings from other rolls. This routine walks through the table and uses
 * the #dk_roll_types::like member to re-use their settings.
 */
int ql_dk_information_get(struct dk_roll_types *medium, const char *name)
{
	const struct dk_roll_types *type, *like;

	type = ql_dk_element_get(name);
	if (type == NULL)
		return -EINVAL;

	*medium = *type;

	while (type->like != NULL) {
		like = ql_dk_element_get(type->like);
		if (like == NULL) {
			caps_print_panic(_("Internal failure. Roll '%s' points to non existing roll '%s'\n"), type->dkname, type->like); /* doc */
			exit(EXIT_FAILURE);
		}
		caps_print_loud(_("Inherit settings from roll '%s'\n"), like->dkname);
		/* use/overwrite the settings from the .like DK type on demand */
		if (like->caps != DK_CAP_UNKNOWN)
			medium->caps = like->caps;
		if (!iszero(like->margin))
			medium->margin = like->margin;
		type = like;
	}

	return 0;
}

/**
 * @page how_to_setup_margins How to setup margins correctly
 *
 * Here a more specific description how to setup the margins if you want to use
 * labels in landscape orientation, but the DK roll provides only portrait
 * orientation.
 *
 * @image html auto_orientation.svg "Landscape label margins on clockwise or counter clockwise rotation"
 *
 * @note The words @b Plus90 and @b Minus90 can be found in the PPD. They are used
 *       by the filter @c pdftopdf to know how to rotate the PDF if the defined orientation
 *       doesn't fit the print orientation. By defining this hint, you can rely
 *       on it, when defining your label and its margins.
 */

/**
 * @page how_the_filter_deals_with_margins How the Filter deals with margins
 *
 * @b Horizontal:
 *
 * If the dot count in a line in the CUPS raster exceeds the maximum of 720 dots
 * the print head has, the filter removes the same amount of dots at the left
 * and right line ends and sends exactly 720 dots to the printer device.
 *
 * If the dot count in a line in the CUPS raster is below the maximum of the
 * selected medium (or the maximum dots of the print head), it will right align
 * the available dots per line and sends the result to the printer device.
 *
 * @b Vertical:
 *
 * For continous labels, all CUPS raster lines are sent to the printer device.
 * You do not need margins at the top and bottom edge. The printer device itself
 * will add 3 mm at both ends and then cut the label.
 *
 * For pre-cut labels the filter always ignores 3 mm of data at the top and at
 * the bottom of the CUPS raster. At 300 DPI this means 35 lines each and at
 * 600 DPI (only some QL models) 70 lines each. The remaining raster lines are
 * sent to the printer device. For this case you need to add these 3 mm margins
 * to your label, e.g. it must provide lines for the full size of the label.
 *
 * @b Example:
 *
 * For a DK1201 roll of pre-cut labels of size 29 mm x 90 mm:
 *
 * - define a label of exact this size
 * - add 3 mm margin at its top and bottom edges
 * - add 1mm margin at its right edge
 * - add 0.5 mm margin at its left edge
 *
 * This label should create a CUPS raster of exact:
 *
 * - 342 dots per line
 * - 1061 lines
 *
 * For a test, if your label fits into this size, you can run the @c pdftoraster
 * filter on your PDF. Safe its output to a file and run the command @c file on
 * it. It should output something like this:
 *
@verbatim
 Cups Raster version 3, Little Endian, 300x300 dpi, 342x1061 pixels 8 bits/color 8 bits/pixel ColorOrder=Chunky ColorSpace=gray
@endverbatim
 *
 * The important part of this output is the *342x1061 pixels*.
 *
 * @attention The line count must match the @a 1061 for the DK1201 roll. The dots
 *            per line value @a 342 can differ (a little bit).
 */

/**
 * Crop the input raster to meet the printer's expectations/requirements
 * @param[in] ql Full job description
 *
 * The printer device has some - hmm, lets it call - interesting requirements
 * about the print data it accepts. Continuous labels are easy, but pre-cut
 * labels aren't. In order to ensure the printer always cuts inbetween two
 * pre-cut labels, we need to tweak the amount of lines to be print.
 *
 * Read @ref how_the_filter_deals_with_margins about details.
*/
int ql_input_raster_crop(struct qldriver *ql)
{
	unsigned dots_on_head = ql->bytes_per_line * 8;
	int rc;

	rc = ql_raster_limits_get(ql);
	if (rc < 0)
		return rc;

	caps_print_loud(_("Raster width: %u dots, raster height: %u dots\n"), ql->trim.raster_width, ql->trim.raster_height);
	caps_print_loud(_("Media width: %u dots, media height: %u dots\n"), ql->trim.media_width, ql->trim.media_height);

	/* Vertical handling based on the media and printer requirements */
	if (ql->pre_sized) {
		/* We must always skip 3 mm of the input raster */
		if (ql->high_density_print)
			ql->trim.top_skip = PRE_CUT_LABEL_MARGIN_SKIP_HIGH_RES; /* 3mm@600 DPI */
		else
			ql->trim.top_skip = PRE_CUT_LABEL_MARGIN_SKIP_LOW_RES; /* 3mm@300 DPI */
		ql->trim.top_keep = ql->trim.raster_height - (2 * ql->trim.top_skip);
		assert(ql->trim.top_keep <= ql->trim.media_height);
	} else {
		/* No real vertical limit for continous length labels */
		ql->trim.top_skip = 0;
		ql->trim.top_keep = ql->trim.raster_height; /* if this line gets changed, both cups_ql_*_page_print() must be adapted */
		/* According to the datasheets there is a limit of 1000 mm */
		assert(ql->trim.top_keep <= ql->trim.media_height);
	}

	/* Horizontal handling based on the media */
	if (ql->trim.raster_width > ql->trim.media_width) {
		caps_print_warn(_("Crop input raster to fit to medium\n")); /* doc */
		ql->trim.left_offset = 0; /* TODO What is the real required offset here to hit the paper? */
		ql->trim.left_skip = (ql->trim.raster_width - ql->trim.media_width) / 2;
		ql->trim.left_keep = ql->trim.media_width; /* Full media line */
	} else {
		ql->trim.left_offset = 0; /* TODO What is the real required offset here to hit the paper? */
		ql->trim.left_skip = 0; /* Full raster line */
		ql->trim.left_keep = ql->trim.raster_width;
	}

	/* Additionally we need to trim to the available dots on our print head */
	if (ql->trim.left_keep > dots_on_head) {
		caps_print_warn(_("Crop input raster to fit to print head\n")); /* doc */
		ql->trim.left_skip = (ql->trim.raster_width - dots_on_head) / 2;
		ql->trim.left_keep = dots_on_head; /* Full print head */
	}

	assert(ql->trim.left_keep <= dots_on_head); /* required by the printer */
	assert(ql->trim.top_keep <= ql->trim.media_height); /* required by the printer */
	assert(ql->trim.top_keep >= 2); /* required by the half tone algorithms */

	caps_print_loud(_("Left skip: %u dots, left_keep: %u dots\n"), ql->trim.left_skip, ql->trim.left_keep);
	caps_print_loud(_("Top skip: %u dots, top_keep: %u dots\n"), ql->trim.top_skip, ql->trim.top_keep);

	return 0;
}

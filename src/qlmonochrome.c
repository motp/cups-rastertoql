/*
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * (c) 2021 Juergen Borleis <projects@caps-printing.org>
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 */
#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "qldriver.h"

/**
 * @file qlmonochrome.c
 * @brief Creating the printer's wire data for monochrome print
 * @copyright GNU General Public License 2.0 or later
 * @author Jürgen Borleis
 *
 * Routines to print monochrome rasters, e.g. black on white. This mode can be
 * used by all label printers.
 */

extern int terminate;

/**
 * Printer command to print monochrome
 *
 * @note @b mode on QL500 series with value 0xff suggests to signal "transmission should be stopped"
 */
struct monochrome_line_start {
	uint8_t command;	/**< 'g' */
	uint8_t mode; /**< 0x00 for monochrome data */
	uint8_t cnt; /**< count of bytes following */
	uint8_t data[0]; /**< line data as payload */
} __packed;

#if 0
/* For reference only, targets a different printer TODO which one? */
struct monochrome_line_start_big {
	uint8_t command;	/**< 'G' */
	uint16_t cnt; /**< count of bytes following in little endian */
	uint8_t data[0]; /**< line data as payload */
} __packed;
#endif

/**
 * Convert one line of data from the half-tone algorithm (e.g. one short int per pixel) into one monochrome line of printer data
 * @param[in] input_cnt Size of elements in @b input
 * @param[in] input The data from the half-tone algorithm
 * @param[out] cmd Where to build the monochrome printer data and its command
 *
 */
static __nonnull((2,3)) void monochrome_line_convert(size_t input_cnt, const signed short input[input_cnt], struct line_command *cmd)
{
	struct monochrome_line_start *wire_data = cmd->command.bw;

	assert((input_cnt % 8) == 0);
	assert(input_cnt < 2041);
	assert(cmd->bytes_per_line >= (input_cnt / 8));

	transform_to_monochrome(input_cnt, input, wire_data->data);

	/* Prepare the command sequence for the printer device */
	wire_data->command = 'g';
	wire_data->mode = 0x00;
	wire_data->cnt = (unsigned char)(input_cnt / 8);
}

/**
 * Allocate the buffer for the monochrome print command
 * @param[in] bytes_per_line Expected bytes per line to be sent to the printer
 * @return #line_command structure
 */
static struct line_command *monochrome_command_get(size_t bytes_per_line)
{
	size_t buffer_cnt = sizeof(struct line_command) + sizeof(struct monochrome_line_start) + bytes_per_line;
	struct line_command *cmd;

	assert(bytes_per_line < 256);

	cmd = malloc(buffer_cnt);
	if (cmd == NULL)
		exit(1); /* no hope */

	cmd->bytes_per_line = bytes_per_line;
	cmd->bytes_per_command = sizeof(struct monochrome_line_start) + bytes_per_line;
	cmd->command.bw = (struct monochrome_line_start *)&cmd[1];

	return cmd;
}

/**
 * Free the buffer for the monochrome print command
 * @param[in] buffer The buffer to be freed
 */
static __nonnull((1)) void monochrome_command_put(void *buffer)
{
	free(buffer);
}

/**
 * Read in the next line from a CUPS monochrome raster right border aligned
 * @param[in,out] ql Full job description
 * @param[in,out] cnv Half tone converter
 * @retval 0 On success
 * @retval -ENODATA Mature end of CUPS raster input data
 *
 * Each raster must be handled right aligned in order to mirror the result prior
 * sending it to the printer.
 *
 * The raster can be wider or smaller than the printing area is. Deal with it.
 *
 * @pre Expected is a raster with one grey byte per dot, 0x00 = black dot, 0xff = no dot
 */
static __nonnull((1,2)) int ql_next_monochrome_line_read(struct qldriver *ql, struct halftone_converter *cnv)
{
	unsigned char line[cnv->pixel_count] __nonstring;
	unsigned char b[ql->trim.raster_width] __nonstring;
	int ret;

	/* Get raw data from the CUPS raster data */
	ret = cups_ql_next_line_read(ql, ql->trim.raster_width, b);
	if (ret < 0)
		return ret;

	/* Clear the whole line, e.g. nothing to print for now */
	memset(line, COLOUR_VAL_BRIGHT, cnv->pixel_count);

	/* Copy the raster content right aligned into the line buffer */
	assert(ql->trim.left_offset + ql->trim.left_keep <= cnv->pixel_count);
	assert(ql->trim.left_skip + ql->trim.left_keep <= ql->trim.raster_width);

	memcpy(&line[cnv->pixel_count - ql->trim.left_keep], &b[ql->trim.left_skip], ql->trim.left_keep);

	move_in_next_line(cnv, line);
	return 0;
}

/**
 * Process one monochrome line and send it to the printer
 * @param[in] ql Full job description
 * @param[in,out] cnv Half tone converter
 * @retval 0 On success
 *
 * @pre The half tone converter must be filled with two lines
 * @pre One of the three available half-tone algorithms must already be selected
 */
static __nonnull((1,2)) int cups_ql_monochrome_line_process(struct qldriver *ql, struct halftone_converter *cnv)
{
	struct line_command *cmd;
	int rc;

	switch (ql->half_tone_type) {
	case HT_ERROR_DIFFUSION:
		halftone_line_with_error_diffusion(cnv);
		break;
	case HT_ORDERED:
		halftone_line_ordered(cnv);
		break;
	case HT_NONE:
		halftone_line_no_dither(cnv);
		break;
	default:
		caps_print_panic(_("Unkown half tone type: %u. Cannot continue.\n"), ql->half_tone_type); /* doc */
		exit(1);
	}

	/* Send the top line to the printer */
	cmd = monochrome_command_get(ql->bytes_per_line);

	monochrome_line_convert(cnv->pixel_count, cnv->sliding_lines[0], cmd);
	rc = ql_printer_data_send(ql, cmd->command.bw, cmd->bytes_per_command, 0);

	monochrome_command_put(cmd);
	return rc;
}

/**
 * Convert the current page into the printer's wire data format
 * @param[in] ql Full job description
 * @param[in] first_time '1' if called the first time in this job, '0' else
 * @retval 0 On success
 * @retval -EINVAL Unsupported raster input format
 * @retval -ECANCELED Termination request from outerspace
 *
 * The routine loops through all or the remaining lines of the current page and
 * converts them into the printer's monchrome wire format
 *
 * @note The #ql_trim::top_keep amount of lines of the input raster is printed.
 *
 * @note In case of an error or a termination request, the print job gets terminated at
 *       the printer's side as well.
 *
 * @pre The format was already checked, e.g. it fits into the printer's media.
 */
int cups_ql_monochome_page_print(struct qldriver *ql, int first_time)
{
	struct halftone_converter cnv;
	int rc = -EINVAL;
	unsigned line;

	caps_print_noisy(_("Entering monochrome page print\n"));

	/* Init the half tone rasterizer */
	cnv.pixel_count = ql->bytes_per_line * 8; /* print head's dots */
	cnv.dotval[0] = 0x00;
	cnv.dotval[1] = 0xff;
	cnv.threshold = 0x80;
	sliding_halftone_get(&cnv);

	assert(ql->trim.top_keep >= 2);

	/* Send the required job header when called the first time in this job */
	if (first_time) {
		rc = ql_job_header_generate(ql);
		if (rc < 0) {
			caps_print_error(_("Monochrome page print failed in job header\n"));
			return rc;
		}
	}

	/* Send the required page header on each call */
	rc = ql_page_header_generate(ql, first_time);
	if (rc < 0) {
		caps_print_error(_("Monochrome page print failed in page header\n"));
		return rc;
	}

	/* Read in the first raster line in a separate manner, because we need two lines to start the half tone algorithm */
	rc = ql_next_monochrome_line_read(ql, &cnv);
	if (rc == 0) {
		/* One loop per page */
		for (line = 1; line < ql->trim.top_keep; line++) {
			rc = ql_next_monochrome_line_read(ql, &cnv);
			if (rc < 0)
				break;
			rc = cups_ql_monochrome_line_process(ql, &cnv);
			if (rc < 0)
				break;

			if (terminate) {
				rc = -ECANCELED;
				break;
			}
		}
	}

	if (rc == 0) {
		/* The last line of the raster is still in the converter as its bottom line. Process it as well */
		move_in_empty_line(&cnv, COLOUR_VAL_BRIGHT);
		rc = cups_ql_monochrome_line_process(ql, &cnv);
	}

	if (rc < 0) {
		/*
		 * Deal with some kind of processing failure and cancel the job if possible:
		 * - on a printer data error, we are out of chance here to send any further data.
		 *   - we need to give up here
		 * - on an input data error, the printer is in a state, where it awaits the next command.
		 *   - here we can send the reset command to cancel the job at the printer's side
		 *
		 * TODO distinguish the failure.
		 */
		rc = ql_reset(ql);
	}

	sliding_halftone_put(&cnv);
	caps_print_noisy(_("Monochrome page print done\n"));
	return rc;
}

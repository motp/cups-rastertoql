Create perfect test documents
=============================

Creating perfect test documents can be done with the help of pyFPDF.

Margins and contents can be set exactly, so these documents are the
reference for testing the rastertoql driver.

The sources of pyFPDF can be found here:

    https://github.com/reingart/pyfpdf.git

After installing them, you can simply run the scripts herein:

    $ python3 dk1209.py

for the DK1209 portrait label.
